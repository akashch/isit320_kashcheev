#!/usr/bin/env node

var debug = require('debug')('SocketBasics');
var http = require('http');
var app = require('../app');
var socketStuff = require('../routes/SocketStuff');

app.set('port', process.env.PORT || 30025);

var server = http.createServer(app);

socketStuff.init(server);

server.listen(app.get('port'), function() {
    'use strict';
    debug('Express server listening on port ' + server.address().port);
});
