/**
 * DataContainer.js
 */
define([ "Core", "utilities" ], function(Core, utilities) {
	'use strict';

	var data = null;
	var score = null;
	var _instance = null;

	function DataContainer(callback) {
		if (_instance === null) {
			_instance = this;
			init(callback);

			Object.defineProperty(this, "data", {
				get : function() {
					return data;
				},
				enumerable : true,
				configurable : true
			});
			
			Object.defineProperty(this, "score", {
				get : function() {
					return score;
				},
				set : function(value) {
					score = value;
				},
				enumerable : true,
				configurable : true
			});

		} else {
			return _instance;
		}

	}

	function init(callback) {
		$.getJSON("GameData.json", function(refData) {
			data = refData;
			callback(data);
		});
	}

	return DataContainer;
});