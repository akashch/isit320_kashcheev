/**
 * Control.js
 */
define(
		[ "Core", "LevelManager", "DataContainer", "SocketControl", "ChatControl", "Floors",
				"PointerLockSetup", "Buildings", "Particles", "utilities" ],
		function(Core, LevelManager, DataContainer, SocketControl, ChatControl, Floors,
				PointerLockSetup, Buildings, Particles, utilities) {
			'use strict';

			var gameMenu = null; // TO DO

			var data = null;
			var core = null;
			var chatControl = null;
			var controls = null;
			var animateNpc = false;

			var position = null;
			var radian = null;
			var length = 2;

			var socketCtrl = null;
			var levelManager = null;
			var dataContainer = null;

			var playerPrevPosition = {};

			var xValue = null;
			var zValue = null;

			function Control() {
				init();
			}

			function init() {

				dataContainer = new DataContainer(
						function(dataRef) {
							data = dataRef;
							instantiate();

							doPointerLock();

							socketCtrl = new SocketControl();

							animate();

							window.addEventListener('resize', onWindowResize,
									false);
							$("#details")
									.html(
											"1. Collect all items </br> 2. Find the door to the next level");
						});

			}

			function instantiate() {
				core = new Core();
				chatControl = new ChatControl();
				levelManager = new LevelManager("lvl0001");// TO DO
			}

			function doPointerLock() {
				controls = new THREE.PointerLockControls(core.camera);

				var yawObject = controls.getObject();
				core.scene.add(yawObject);

				// Move camera to the 1, 1 position
				yawObject.position.x = core.size;
				yawObject.position.z = core.size;

				var ps = new PointerLockSetup(controls);
			}

			function animate() {

				var controlObject = controls.getObject();
				position = controlObject.position;
				
				radian =  -controlObject.rotation.y;

				socketCtrl.sendData2Listener(position, radian);
				reDrawTinyMap();

				requestAnimationFrame(animate);
				var xAxis = new THREE.Vector3(1, 0, 0);
				levelManager.particles.rotateParticlesAroundWorldAxis(0, xAxis,
						Math.PI / 180, animateNpc);
				animateNpc = !animateNpc;

				controls.isOnObject(false);

				levelManager.buildings.collisionDetection(position, controls);

				// Move the camera
				controls.update();

				core.renderer.render(core.scene, core.camera);

			}

			function reDrawTinyMap() {

				// MAP
				utilities.comparePisiton(position);

				xValue = Math.round(position.x / core.size);
				zValue = Math.round(position.z / core.size);
				drawMap(xValue, zValue);

				// Player
				//utilities.drawTinyMap(xValue, zValue, "#00FFFF");
				utilities.drawTriangle(xValue, zValue, radian, "#00FFFF");
				
				
				/* jshint ignore:start */
				if (!utilities.isEmpty(core.grid)
						&& utilities.isDefined(playerPrevPosition.x)
						&& utilities.isDefined(playerPrevPosition.z)
						&& utilities.isDefined(core.grid[playerPrevPosition.x])
						&& utilities
								.isDefined(core.grid[playerPrevPosition.x][playerPrevPosition.z])
						&& !(utilities.isEqual(xValue, playerPrevPosition.x) && utilities
								.isEqual(zValue, playerPrevPosition.z))) {
					switch (core.grid[playerPrevPosition.x][playerPrevPosition.z]) {
					case 1:
						utilities.drawTinyMap(playerPrevPosition.x,
								playerPrevPosition.z, "#FF0000");
						break;
					case 0:
						utilities.drawTinyMap(playerPrevPosition.x,
								playerPrevPosition.z, "#0000FF");
						break;
					}
				}
				/* jshint ignore:end */

				playerPrevPosition = {
					"x" : xValue,
					"z" : zValue
				};

				// NPCS
				utilities.iterate(core.npcs, function(i, j) {
					if (core.npcs[j][i] !== 0) {
						utilities.drawTinyMap(i, j, "#FFFF00");
					}
				});

			}

			function drawMap(x, z) {
				for (var i = 0; i < (2 * length + 1); i++) {
					for (var j = 0; j < (2 * length + 1); j++) {
						helper(x + (i - length), z + (j - length));
					}
				}
			}

			function helper(x, z) {
				if (!utilities.isEmpty(core.grid) && utilities.isDefined(core.grid[x]) && utilities.isDefined(core.grid[x][z])) {
					if (core.grid[x][z] == 1)
						utilities.drawTinyMap(x, z, "#FF0000");
					else {
						utilities.drawTinyMap(x, z, "#0000FF");
					}
					if (playerPrevPosition.x !== x && playerPrevPosition.z !== z) {
						$.publish("updateScore", {
							"xValue" : x,
							"zValue" : z
						});
					}
				}
			}

			function onWindowResize() {
				core.camera.aspect = window.innerWidth / window.innerHeight;
				core.camera.updateProjectionMatrix();
				core.renderer.setSize(window.innerWidth, window.innerHeight);
			}

			return Control;
		});
