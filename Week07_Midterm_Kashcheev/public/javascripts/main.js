/**
 * main.js
 */
require.config({
    baseUrl: '.',
    paths: {
        "jquery": 'javascripts/lib/jquery',
        "TinyPubSub": "javascripts/lib/TinyPubSub",
        "Three": 'javascripts/lib/three',
        "MTLLoader": 'javascripts/lib/MTLLoader',
        "OBJMTLLoader": 'javascripts/lib/OBJMTLLoader',
        "ColladaLoader": 'javascripts/lib/ColladaLoader',
        "Core": "javascripts/controls/Core",
        "ChatControl" : "javascripts/controls/ChatControl",
        "GameMenu": "javascripts/GameMenu",
        "Control": 'javascripts/controls/Control',
        "DataContainer" : "javascripts/controls/DataContainer",
        "LevelManager" : 'javascripts/controls/LevelManager',
        "SocketControl": "javascripts/controls/SocketControl",
        "Floors": 'javascripts/draw/Floors',
        "PointerLockControls": "javascripts/lib/PointerLockControls",
        "PointerLockSetup": "javascripts/controls/PointerLockSetup",
        "utilities": "javascripts/utilities",
        "Particles": "javascripts/draw/Particles",
        "Buildings": "javascripts/draw/Buildings",
        "Shapes": "javascripts/draw/Shapes"
    },
    shim: {
        'Three': {
            exports: 'Three'
        },
        'PointerLockControls': {
            exports: 'PointerLockControls'
        },
        'MaterialLoader': {
            exports: 'MTLLoader'
        },
        'OBJMTLLoader': {
            exports: 'OBJMTLLoader'
        },
        'ColladaLoader': {
            exports: 'ColladaLoader'
        },
        "TinyPubSub": {
            deps: ["jquery"],
            exports: "TinyPubSub"
        }

    }
});

require(['jquery', 'TinyPubSub', 'Three', 'Core', 'Control', 'DataContainer', 'LevelManager', 'GameMenu', 'SocketControl', 'Floors', 'PointerLockSetup', 'utilities', 'Particles', 'Buildings', 'Shapes', 'utilities'],
    function(jq, TinyPubSub, Three, Core, Control, DataContainer, LevelManager, GameMenu, SocketControl, Floors, PointerLockSetup, Particles, Buildings, Shapes, util) {
        'use strict';

        $(document).ready(function() {
            var control = new Control();
            //var gameMenu = new GameMenu();

        });




    });
