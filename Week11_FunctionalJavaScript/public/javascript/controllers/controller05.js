/**
 * Controller
 */

mainModule.controller("controller05", function($scope, myFactory01) {
	"use strict";
	
	myFactory01.get("p05").then(function(names){
		$scope.names = names;
	});
	
	
});