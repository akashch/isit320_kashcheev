/**
 * Controller
 */

var mainModule = angular.module("mainModule", ['ngRoute']);


mainModule.config(function($routeProvider) {
	"use strict";
	$routeProvider.when("/", {
		controller : "controller01",
		templateUrl : "partials/view01.html"
	});
	$routeProvider.when("/view01", {
		controller : "controller01",
		templateUrl : "partials/view01.html"
	});
	$routeProvider.when("/view02", {
		controller : "controller02",
		templateUrl : "partials/view02.html"
	});
	$routeProvider.when("/view03", {
		controller : "controller03",
		templateUrl : "partials/view03.html"
	});
	$routeProvider.when("/view04", {
		controller : "controller04",
		templateUrl : "partials/view04.html"
	});
	$routeProvider.when("/view05", {
		controller : "controller05",
		templateUrl : "partials/view05.html"
	}).otherwise({
		redirectTo : '/'
	});
});


mainModule.controller("controller01", function($scope, myFactory01) {
	"use strict";
	
	
	myFactory01.get("p01").then(function(states){
		$scope.states = states;
	});
	
	
});
