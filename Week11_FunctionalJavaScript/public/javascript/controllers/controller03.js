/**
 * Controller
 */

mainModule.controller("controller03", function($scope, myFactory01) {
	"use strict";
	
	myFactory01.get("p03").then(function(names){
		$scope.names = names;
	});
	
	
	$scope.startWith = function(){
		$scope.names = myFactory01.startWith($scope.letter);
		$scope.letter = "";
	};
	
});