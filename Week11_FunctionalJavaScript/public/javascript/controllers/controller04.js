/**
 * Controller
 */

mainModule.controller("controller04", function($scope, myFactory01) {
	"use strict";
	
	myFactory01.get("p04").then(function(names){
		$scope.names = names;
	});
	
	
});