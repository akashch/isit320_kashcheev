/**
 * Controller
 */

mainModule.controller("controller02", function($scope, myFactory01) {
	"use strict";
	
	myFactory01.get("p02").then(function(states){
		$scope.states = states;
	});
	
	
});