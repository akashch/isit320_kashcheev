/**
 * Factory
 */

var mainModule = angular.module("mainModule");

mainModule.factory("myFactory01", function($q) {
	"use strict";

	var exports = {};

	var names = [];
	var states = [];

	Array.prototype.filter = function(predicateFunction) {
		var results = [];

		this.forEach(function(itemInArray) {
			if (predicateFunction(itemInArray)) {
				results.push(itemInArray);
			}
		});

		return results;
	};

	Array.prototype.mergeAll = function() {
		var results = [];
		this.forEach(function(subArray) {
			console.log(subArray);
			subArray.forEach(function(number) {
				results.push(number);
			});
		});
		return results;
	};

	exports.startWith = function(letter) {
		console.log(letter);
		return names.filter(function(name) {
			return name.indexOf(letter) === 0
					|| name.indexOf(letter.toUpperCase()) === 0;
		});
	};
	
	var theSwitch = {
			p01: function(defers){
				ajaxCall('json/States.json', function(response) {
					var result = [];

					response.docs.forEach(function(state) {
						result.push("{ " + state.name + ", " + state.abbreviation
								+ "}");
					});

					defers.resolve(result);
				});
				
			},
			p02 : function(defers){
				ajaxCall('json/States.json', function(response) {
					var result = [];

					response.docs.forEach(function(state) {
						result.push("{ " + state.name + ", " + state.abbreviation
								+ "}");
					});

					defers.resolve(result);
				});
				
			},
			p03 : function(defers){
				
				ajaxCall('json/names.json', function(response) {
					names = response;
					defers.resolve(response);
				});
				
			},
			p04 : function(defers){
				
				ajaxCall('json/presidents.json', function(response) {
					var result = [];

					result = response.map(function(data) {
						return data.states;
					}).mergeAll();

					defers.resolve(result);
				});
				
			},
			p05 : function(defers){
				
				ajaxCall('json/presidents.json', function(response) {

					var result = [];

					result = response.map(function(data) {
						return data.states;
					}).mergeAll().map(
							function(item) {
								return "{ " + item.state.name + ", "
										+ item.state.abbreviation + "}";
							});

					defers.resolve(result);
				});
				
			}
			
	}

	exports.get = function(part) {
		var defers = $q.defer();
		
		theSwitch[part](defers);
		
		return defers.promise;
	};


	// *****************************
	// AJAX Call
	// *****************************
	function ajaxCall(fileName, callback) {

		$.ajax({
			type : 'GET',
			url : fileName,
			dataType : 'json',
			success : function(response) {
				callback(response);
			},
			error : showError
		});

	}

	// *****************************
	// utilities
	// *****************************

	var showError = function(request, ajaxOptions, thrownError) {
		console.log("*****************");
		console.log("Error occurred: = " + ajaxOptions + " " + thrownError);
		console.log(request.status);
		console.log(request.statusText);
		console.log(request.getAllResponseHeaders());
		console.log(request.responseText);
		console.log("*****************");
	};

	return exports;

});