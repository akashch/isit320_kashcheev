/**
 * Particles.js
 */
define(["Core", "Score", "Shapes", "utilities"],
    function(Core, Score, Shapes, utilities) {
        'use strict';

        var particles = [];
        var npcs = [];
        var that = {};
        var shapes;
        var core;
        var grid;
        var score;


        function Particles() {
            // instantiated
            shapes = new Shapes();
            core = new Core();
            score = new Score();
        }

        function showParticles(x, y) {

            var cloud = score.npcData[score.count++];

            var geometry = new THREE.IcosahedronGeometry(10, 2);
            var material = new THREE.PointCloudMaterial({
                color: cloud.value.color,
                size: 0.2
            });

            var particleSystem = new THREE.PointCloud(geometry, material);
            particleSystem.position.set(x, 10, y);
            particleSystem.name = utilities.getName("PCloud", x, y);

            particleSystem.couchData = cloud;

            if (score.count === score.npcData.length) {
                score.count = 0;
            }

            core.scene.add(particleSystem);

            core.clouds.push(particleSystem);

            particles.push(particleSystem);
        }

        Particles.prototype.rotateParticlesAroundWorldAxis = function(
            npcIndex, axis, radians, npc) {
            if (npcs.length > 0) {
                for (var i = 0; i < npcs.length; i++) {
                    var object;
                    if (npc === true) {
                        object = npcs[i];
                    } else {
                        object = particles[i];
                    }

                    that.rotWorldMatrix = new THREE.Matrix4();
                    that.rotWorldMatrix.makeRotationAxis(axis.normalize(),
                        radians);

                    that.rotWorldMatrix.multiply(object.matrix); // pre-multiply

                    object.matrix = that.rotWorldMatrix;

                    object.rotation.setFromRotationMatrix(object.matrix);
                }
            }
        };

        Particles.prototype.initNpc = function(fileName) {
            $.ajax({
                url: fileName,
                cache: false,
                type: "GET",
                dataType: "json",
                success: function(gridData) {
                    grid = gridData;
                    core.npcs = gridData;
                    utilities.showDebug('Opening file: ' + fileName);

                    utilities.iterate(gridData, function(i, j) {
                        var npcValue = gridData[i][j];
                        if (npcValue !== 0) {
                            core.numberOfNpcs++;
                            core.originalNumberOfNpcs++;
                            shapes.addStarObject(npcs, false, j * core.size, i * core.size);
                            showParticles(j * core.size, i * core.size);
                        }
                    });
                },

                error: utilities.showError
            });
        };

        return Particles;
    });
