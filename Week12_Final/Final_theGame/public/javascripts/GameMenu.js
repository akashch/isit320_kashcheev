/**
 * gameMenu.js
 */

define(["Control"], function(Control) {
    'use strict';
    var _instance = null;

    function GameMenu() {
        if (_instance === null) {
            _instance = this;
            init();
        } else {
            return _instance;
        }
    }

    function init() {
        dispalyMainMenu();
        $.subscribe("showPopBox", function(event, eventArgs) {
            showPopBox(eventArgs);
        });
    }

    function showPopBox(data) {
        $("#container").load('pages/popBox.htm #popBox');
        $("#popBox").click(function() {
            dispalyMainMenu();
        });

    }

    function dispalyMainMenu() {
        $("#container").load('pages/menu.htm', function() {
            $(".levels").click(function(data) {
                $("#container").empty();
                $("#container").load('pages/game.htm');
                control = new Control(data.toElement.id);
            });
        });
    }


    return GameMenu;
});
