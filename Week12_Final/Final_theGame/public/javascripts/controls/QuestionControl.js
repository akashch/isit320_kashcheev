/**
 * New node file
 */
define(
    [require],
    function() {
        'use strict';

        var _instance = null;
        var answer;

        function QuestionControl() {
            if (_instance === null) {
                _instance = this;
                init();

                Object.defineProperty(this, "answer", {
                    get: function() {
                        return answer;
                    },
                    set: function(value) {
                        answer = value;
                    },
                    enumerable: false,
                    configurable: true
                });

            } else {
                return _instance;
            }
        }

        function init() {
            $(document).keydown(onkeydown);
        }

        function onkeydown(event) {

            switch (event.keyCode) {

                case 84:
                    answer = true;
                    console.log(answer);
                    break;
                case 70:
                    answer = false;
                    console.log(answer);
                    break;
            }
        }

        QuestionControl.prototype.getAnswer = function() {
            if (answer !== null) {
                return answer;
            }
        };

        QuestionControl.prototype.setAnswer = function(value) {
            answer = value;
        };

        QuestionControl.prototype.askQuestion = function(npc) {

            $("#question").show("fast");
            $("#question p").html("Here is questions from " + npc.couchData.value.name + " </br> <strong>Question:</strong> " + npc.couchData.value.question + "</br>Press 'T' for <strong>TRUE</strong> or 'F' for <strong>FALSE</strong> ");

        };

        return QuestionControl;
    });
