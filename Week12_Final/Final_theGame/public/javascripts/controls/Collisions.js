/**
 * w * Collisions.js
 */
define(['Core', 'Score', 'QuestionControl', "utilities"],
    function(Core, Score, QuestionControl, utilities) {
        'use strict';

        var core = null;
        var score = null;
        var qCtrl = null;
        var _instance = null;

        function Collisions() {
            if (_instance === null) {
                _instance = this;
                init();
            } else {
                return _instance;
            }
        }

        function init() {
            core = new Core();
            score = new Score();
            qCtrl = new QuestionControl();
        }

        Collisions.prototype.collisionDetection = function(position,
            controls) {

            var rays = [new THREE.Vector3(0, 0, 0.1),
                new THREE.Vector3(0.1, 0, 0.1),
                new THREE.Vector3(0.1, 0, 0),
                new THREE.Vector3(0.1, 0, -0.1),
                new THREE.Vector3(0, 0, -0.1),
                new THREE.Vector3(-0.1, 0, -0.1),
                new THREE.Vector3(-0.1, 0, 0),
                new THREE.Vector3(-0.1, 0, 0.1)
            ];

            position = controls.getObject().position;
            var rayHits = [];

            for (var i = 0; i < rays.length; i++) {
                core.raycaster.set(position, rays[i]);
                var intersections = core.raycaster
                    .intersectObjects(core.cubes);
                if (intersections.length > 0) {

                    controls.isOnObject(true);
                    rayHits.push(i);
                }

                if (core.raycaster.intersectObjects(core.clouds).length > 0 && core.raycaster.intersectObjects(core.clouds)[0].distance < 15) {
                    //utilities.removeNpc(core.raycaster.intersectObjects(core.clouds)[0].object)
                    if (mainCharacterStrongerThanNpc()) {

                        utilities.removeNpc(core.raycaster.intersectObjects(core.clouds)[0].object);
                        controls.isOnObject(true);

                    } else {
                        rayHits.push(i);
                    }
                    controls.isOnObject(true);

                }
            }



            if (rayHits.length > 0) {
                var dir = controls.getDirection(new THREE.Vector3(0, 0, 0))
                    .clone();
                bounceAway(position, rays, rayHits);
            }

            return false;
        };

        function mainCharacterStrongerThanNpc() {
            var npcs_value = parseInt(core.raycaster.intersectObjects(core.clouds)[0].object.couchData.value.value);

            if (npcs_value <= score.mainCharacter.value) {

                qCtrl.askQuestion(core.raycaster.intersectObjects(core.clouds)[0].object);
                if (typeof qCtrl.getAnswer() !== 'undefined' && ((qCtrl.getAnswer() && JSON.parse(core.raycaster.intersectObjects(core.clouds)[0].object.couchData.value.answer)) || (!qCtrl.getAnswer() && !JSON.parse(core.raycaster.intersectObjects(core.clouds)[0].object.couchData.value.answer)))) {

                    score.mainCharacter.value += npcs_value;
                    $("#question").hide(1000);
                    qCtrl.setAnswer(null);

                    return true;
                }
            } else {
                return false;
            }

        }

        function bounceAway(position, rays, rayHits) {
            var choice = 0;
            if (rayHits.lenght > 1) {
                choice = utilities.average(rayHits);
            }
            var hitChoice = rayHits[choice];

            var newDir = (hitChoice + 3) % 7;
            doMove(position.x + (rays[newDir].x), position.z + (rays[newDir].z), position);

        }

        function doMove(newX, newZ, position) {
            position.x = newX;
            position.z = newZ;

        }

        return Collisions;

    });
