/**
 * SocketControl.js (Singelton)
 */
define(["Core"], function(Core) {
    'use strict';

    var socketGame = null;
    var socketListener = null;

    var _instance = null;
    var core = null;

    function SocketControl() {

        if (_instance === null) {
            _instance = this;

            core = new Core();
            init();
        } else {
            return _instance;
        }

    }

    function init() {
        socketGame = io.connect('http://localhost:30025');

        socketListener = io.connect('http://localhost:30026');

        socketListener.on('socket_is_connected', function(message) {
            console.log(message);
        });

        socketGame.on('socket_is_connected', function(message) {
            console.log(message);
        });

        socketListener.on('chatMessage', function(msg) {
            $.publish("chatMessageRecieved", {
                "message": msg
            });
        });

        socketListener.on('dbData', function(data) {
            $.publish("dbDataReceved", data);
        });

        $.subscribe("sendChatMessage2Listener", sendChatMessage2Listener);

        $.subscribe("queryDb", queryDb);

        //It would be probably easier just call sendChatMessage2Listener() directly from here
        $.publish("bigEvent", {
            "message": "The game is started!",
            "details": "The game is started!",
            "isStarted": true
        });


    }

    function queryDb(event, eventArgs) {
        socketGame.emit("queryDb", eventArgs);

    }

    function sendChatMessage2Listener(event, eventArgs) {
        socketGame.emit("sendChatData2Listener", {
            "message": eventArgs.message,
            "details": eventArgs.details,
            "isStarted": eventArgs.isStarted
        });
    }

    SocketControl.prototype.sendData2Listener = function(position, radian) {
        socketGame.emit("sendData2Listener", {
            "position": position,
            "grid": core.grid,
            "npcs": core.npcs,
            "numberOfNpcs": core.numberOfNpcs,
            "numberOfExplored": core.numberOfExplored,
            "radian": radian
        });
        socketGame.emit("chatMessage", {
            "message": "debug"
        });

    };

    return SocketControl;
});
