/**
 * Score.js
 */
define([require], function() {
    'use strict';
    var _instance = null;
    var npcData = [];
    var mainCharacter = {
        name: "Robin",
        value: 15
    };
    var count = 0;
    var done;
    var colors = [];

    function Score(callback) {

        if (_instance === null) {
            _instance = this;
            init();
            done = callback;
            Object.defineProperty(this, "npcData", {
                get: function() {
                    return npcData;
                },
                set: function(arr) {
                    npcData = arr;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "mainCharacter", {
                get: function() {
                    return mainCharacter;
                },
                set: function(obj) {
                    mainCharacter = obj;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "count", {
                get: function() {
                    return count;
                },
                set: function(value) {
                    count = value;
                },
                enumerable: true,
                configurable: true
            });

        } else {
            return _instance;
        }

    }

    function init() {
        console.log("init called");
        $.publish("queryDb", {
            message: "retrive npcs data",
            endpoint: "view",
            docName: "npcs",
            view: "value"
        });
        $.subscribe("dbDataReceved", storeDbData);
    }

    function storeDbData(event, eventArgs) {

        npcData = eventArgs.data;
        done();
    }

    return Score;
});
