/**
 *
 * Core.js (Singelton pattern)
 *
 */

define([require], function() {
    'use strict';

    var _instance = null;

    var size = null;
    var scene = null;
    var renderer = null;
    var camera = null;
    var raycaster = null;
    var baseNames = ["Cube", "Star", "PCloud"];
    var flag = false;

    var removedZ = 60;
    var removedX = 0;
    var endOfTheGame = false;

    var grid = [];
    var npcs = [];

    var cubes = [];
    var clouds = [];

    var colors = [];

    var numberOfNpcs = 0;
    var originalNumberOfNpcs = 0;

    function Core() {

        if (_instance === null) {
            _instance = this;
            Object.defineProperty(this, "scene", {
                get: function() {
                    return scene;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "camera", {
                get: function() {
                    return camera;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "renderer", {
                get: function() {
                    return renderer;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "raycaster", {
                get: function() {
                    return raycaster;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "grid", {
                get: function() {
                    return grid;
                },
                set: function(arr) {
                    grid = arr;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "cubes", {
                get: function() {
                    return cubes;
                },
                set: function(arr) {
                    cubes = arr;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "clouds", {
                get: function() {
                    return clouds;
                },
                set: function(arr) {
                    clouds = arr;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "removedZ", {
                get: function() {
                    return removedZ;
                },
                set: function(value) {
                    removedZ = value;
                },
                configurable: true
            });

            Object.defineProperty(this, "removedX", {
                get: function() {
                    return removedX;
                },
                set: function(value) {
                    removedX = value;
                },
                configurable: true
            });

            Object.defineProperty(this, "endOfTheGame", {
                get: function() {
                    return endOfTheGame;
                },
                configurable: true
            });

            /* --------- */

            Object.defineProperty(this, "npcs", {
                get: function() {
                    return npcs;
                },
                set: function(arr) {
                    npcs = arr;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "numberOfNpcs", {
                get: function() {
                    return numberOfNpcs;
                },
                set: function(number) {
                    numberOfNpcs = number;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "originalNumberOfNpcs", {
                get: function() {
                    return originalNumberOfNpcs;
                },
                set: function(number) {
                    originalNumberOfNpcs = number;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(this, "size", {
                get: function() {
                    return size;
                },
                set: function(number) {
                    size = number;
                },
                enumerable: true,
                configurable: true
            });

            init();
        } else {
            return _instance;
        }
    }

    function init() {
        var screenWidth = window.innerWidth / window.innerHeight;
        camera = new THREE.PerspectiveCamera(75, screenWidth, 1, 1000);

        scene = new THREE.Scene();
        scene.fog = new THREE.Fog(0xffffff, 0, 600);
        raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3(
            0, -1, 0), 0, 10);

        renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);
        $.subscribe("updateTinyMap", function(event, eventArgs) {
            updateScene(eventArgs.xPosition, eventArgs.zPosition);
        });

        $.subscribe("removeCompletedLevel", removeCompletedLevel);

    }

    function removeCompletedLevel(event, eventArgs) {
        // console.log(eventArgs.message);
        cubes = [];
        clearScene();
        if (!flag) {
            flag = true;
            setTimeout(function() {
                console.log(cubes);
                grid = [];
                npcs = [];
                $.publish("loadNewLevel", {
                    message: "LOAD NEW LEVEL",
                    "value": "Value02"
                });
            }, 1000);
        }

    }

    function clearScene() {
        var children = scene.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i].hasOwnProperty('name') && children[i].name !== "") {
                removeObject(children[i]);
            }
        }
    }

    function updateScene(xValue, zValue) {
        scene.remove(scene
            .getObjectByName(getName(baseNames[1], xValue, zValue)));
        scene.remove(scene
            .getObjectByName(getName(baseNames[2], xValue, zValue)));
        checkNumberOfNpcs(baseNames[0]);
    }

    function removeObjectbyName(baseName, xValue, zValue) {
        scene.remove(scene.getObjectByName(getName(baseName, xValue, zValue)));
    }

    function removeObject(object) {
        scene.remove(object);
    }

    function checkNumberOfNpcs() {
        $.publish("bigEvent", {
            "message": "New Level",
            "details": "Number of left NPCS is " + numberOfNpcs
        });
        if (--numberOfNpcs === 0) {

            removeObjectbyName(baseNames[0], removedX, removedZ);
            grid[removedX / size][removedZ / size] = 5;

            cubes.splice(cubes.indexOf($.grep(cubes, function(cube) {
                return cube.name === getName(baseNames[0], removedX, removedZ);
            }).pop()), 1);

            endOfTheGame = true;
            $("#result").html(
                "<strong>THE DOOR TO THE NEXT LEVEL IS OPEN!</strong>");
        }
    }

    // This method needs to be created here because when I define utilities,
    // it makes Core is undefined under utilities.
    function getName(baseName, x, z) {
        return baseName + x + z;
    }
    return Core;
});
