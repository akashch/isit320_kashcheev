/**
 * Control.js
 */
define(
    ["Core", "Score", "LevelManager", "Collisions", "DataContainer",
        "SocketControl", "ChatControl", "Floors", "PointerLockSetup",
        "Buildings", "Particles", "utilities"
    ],
    function(Core, Score, LevelManager, Collisions, DataContainer,
        SocketControl, ChatControl, Floors, PointerLockSetup,
        Buildings, Particles, utilities) {
        'use strict';

        var gameMenu = null; // TO DO

        var data = null;
        var core = null;
        var score = null;
        var chatControl = null;
        var controls = null;
        var animateNpc = false;

        var position = null;
        var radian = null;
        var length = 2;

        var socketCtrl = null;
        var levelManager = null;
        var dataContainer = null;
        var collisions = null;

        var playerPrevPosition = {};

        var xValue = null;
        var zValue = null;

        function Control() {
            init();
        }

        function init() {

            socketCtrl = new SocketControl();
            score = new Score(
                function() {
                    dataContainer = new DataContainer(
                        function(dataRef) {
                            data = dataRef;
                            instantiate();

                            doPointerLock();

                            animate();

                            window.addEventListener('resize',
                                onWindowResize, false);
                            $("#details")
                                .html(
                                    "1. Collect all items </br> 2. Player's initial power 15. After collecting NPC, its power would be added player's power </br> NPC Power: Yellow - 15, Green - 45, Blue - 135 </br> 3. Find the door to the next level </br> Press 'R' to hide the minimap");

                        });
                });

        }

        function instantiate() {

            core = new Core();
            collisions = new Collisions();
            chatControl = new ChatControl();
            levelManager = new LevelManager("lvl0001"); // TO DO
            $.subscribe("setOnStart", function(event, evantArgs) {
                position.x = 20;
                position.z = 20;
            });
        }

        function doPointerLock() {
            controls = new THREE.PointerLockControls(core.camera);

            var yawObject = controls.getObject();
            core.scene.add(yawObject);

            // Move camera to the 1, 1 position
            yawObject.position.x = core.size;
            yawObject.position.z = core.size;

            var ps = new PointerLockSetup(controls);
        }

        function animate() {

            var controlObject = controls.getObject();
            position = controlObject.position;
            radian = -controlObject.rotation.y;

            socketCtrl.sendData2Listener(position, radian);
            reDrawTinyMap();

            requestAnimationFrame(animate);
            var xAxis = new THREE.Vector3(1, 0, 0);
            levelManager.particles.rotateParticlesAroundWorldAxis(0, xAxis,
                Math.PI / 180, animateNpc);
            animateNpc = !animateNpc;

            controls.isOnObject(false);

            collisions.collisionDetection(position, controls);

            // Move the camera
            controls.update();

            core.renderer.render(core.scene, core.camera);

        }

        function reDrawTinyMap() {

            // MAP
            utilities.comparePisiton(position);

            xValue = Math.round(position.x / core.size);
            zValue = Math.round(position.z / core.size);
            drawMap(xValue, zValue);

            // Player
            utilities.drawTriangle(xValue, zValue, radian, "#00FFFF");

            playerPrevPosition = {
                "x": xValue,
                "z": zValue
            };

            // NPCS
            utilities.iterate(core.npcs, function(i, j) {
                if (core.npcs[j][i] !== 0) {
                    var color = utilities.getColor(i * 20 + "" + j * 20);
                    utilities.drawTinyMap(i, j, color);
                }
            });

        }

        function drawMap(x, z) {
            for (var i = 0; i < (2 * length + 1); i++) {
                for (var j = 0; j < (2 * length + 1); j++) {
                    helper(x + (i - length), z + (j - length));
                }
            }
        }

        function helper(x, z) {
            if (!utilities.isEmpty(core.grid) && utilities.isDefined(core.grid[x]) && utilities.isDefined(core.grid[x][z])) {
                if (core.grid[x][z] == 1)
                    utilities.drawTinyMap(x, z, "#FF0000");
                else {
                    utilities.drawTinyMap(x, z, "#0000FF");
                }
                if (playerPrevPosition.x !== x && playerPrevPosition.z !== z) {
                    $.publish("updateScore", {
                        "xValue": x,
                        "zValue": z
                    });
                }
            }
        }

        function onWindowResize() {
            core.camera.aspect = window.innerWidth / window.innerHeight;
            core.camera.updateProjectionMatrix();
            core.renderer.setSize(window.innerWidth, window.innerHeight);
        }

        return Control;
    });
