/**
 * SocketStuff.js
 */
module.exports = {

    init: function(server) {
        console.log("init called");
        var gameData = null;

        var io = require('socket.io').listen(server);

        io.sockets.on('connection', function(socket) {
            socket.emit('socket_is_connected', 'theGame is connected to the server!');

            socket.on('sendData2Listener', function(data) {
                gameData = data;
                io.sockets.emit('sendGameData', gameData);
            });

            socket.on('queryDb', function(data) {
                console.log(data);
                io.sockets.emit('query', data);
            });

            socket.on('sendChatData2Listener', function(data) {
                gameData = data;
                io.sockets.emit('chatMessage', gameData);
            });



        });


    }

};
