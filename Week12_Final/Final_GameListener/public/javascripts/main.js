/**
 * main.js
 */
require.config({
    baseUrl: '.',
    paths: {
        "jquery": 'javascripts/lib/jquery',
        "TinyPubSub": "javascripts/lib/TinyPubSub",
        "Control": 'javascripts/controls/Control',
        "SocketControl": "javascripts/controls/SocketControl",
        "DataContainer": "javascripts/container/DataContainer",
        "DatabaseControl": "javascripts/controls/DatabaseControl",
        "utilities": "javascripts/utilities"
    },
    shim: {
        "TinyPubSub": {
            deps: ["jquery"],
            exports: "TinyPubSub"
        }

    }
});

require(['jquery', 'TinyPubSub', 'Control', 'SocketControl', 'DataContainer', 'utilities'],
    function(jq, TinyPubSub, Control, SocketControl, DataContainer, util) {
        'use strict';

        $(document).ready(function() {
            var control = new Control();
        });

    });
