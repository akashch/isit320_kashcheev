/**
 * Control.js
 */
define(["SocketControl", "DatabaseControl", "DataContainer", "utilities"], function(
    SocketControl, DatabaseControl, DataContainer, utilities) {
    'use strict';
    var socketControl = null;
    var dataContainer = null;
    var dbControl = null;

    function Control() {
        dataContainer = new DataContainer();
        socketControl = new SocketControl(this);
        init();
    }

    function init() {
        dbControl = new DatabaseControl();
    }

    Control.prototype.reDrawTinyMap = function() {

        utilities.iterate(dataContainer.data.grid, function(i, j) {
            // console.log(dataContainer.data.grid[i][j]);
            if (dataContainer.data.grid[i][j] === 1) {
                utilities.drawTinyMap(i, j, "#FF0000");
            } else {
                utilities.drawTinyMap(i, j, "#0000FF");
            }
        });

        // Player
        utilities.drawTriangle(Math.round(dataContainer.data.position.x) / 20, Math.round(dataContainer.data.position.z) / 20, dataContainer.data.radian, "#00FFFF");
        // NPCS
        utilities.iterate(dataContainer.data.npcs, function(i, j) {
            if (dataContainer.data.npcs[j][i] !== 0) {
                utilities.drawTinyMap(i, j, "#FFFF00");
            }
        });


        $("#debug").html(
            "X : " + Math.round(dataContainer.data.position.x) + " <br>Y : " + Math.round(dataContainer.data.position.y) + "<br>Z : " + Math.round(dataContainer.data.position.z) + "<br> Number of Living NPCS : " + (dataContainer.data.numberOfNpcs) + "<br> Score: " + dataContainer.data.numberOfExplored);

    };

    return Control;
});
