/**
 * Database Control
 */
define([require], function() {
    'use strict';

    var _instance = null;

    function DatabaseControl() {
        if (_instance === null) {
            //init();
            $("#createDb").click(createDb);
            $("#deleteDb").click(deleteDb);
            $("#insertDocs").click(init);


            $.subscribe("queryDb", query);
            _instance = this;
        } else {
            return _instance;
        }
    }

    function init() {
        readJson();
        createDesignDoc();


    }


    function query(event, eventArgs) {


        var url = "/" + eventArgs.endpoint + "?docName=" + eventArgs.docName + "&view=" + eventArgs.view;

        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(response) {
                $.publish("sendResponse", {
                    data: response.msg.rows
                });
            },
            error: showError
        });

    }

    function createDb() {
        $.ajax({
            type: 'GET',
            url: '/create',
            dataType: 'json',
            success: function(response) {
                displayResponse(response);
            },
            error: showError
        });
    }

    function deleteDb() {
        $.ajax({
            type: 'GET',
            url: '/delete',
            dataType: 'json',
            success: function(response) {
                displayResponse(response);
            },
            error: showError
        });
    }


    function readJson() {

        $.ajax({
            type: 'GET',
            url: '/data/npcsObjects.json',
            dataType: 'json',
            success: function(response) {
                var i = 1;
                response.forEach(function(json) {
                    loadData("Cloud00" + i, json);
                    i++;
                });
            },
            error: showError
        });

    }

    function loadData(docName, json) {

        $.ajax({
            type: 'GET',
            url: '/load',
            dataType: 'json',
            data: {
                docName: docName,
                data: json
            },
            success: function(response) {
                console.log(response);
                displayResponse(response);
            },
            error: showError
        });

    }

    function getDocNames() {

        $.ajax({
            type: 'GET',
            url: '/docNames',
            dataType: 'json',
            success: function(response) {
                console.log(response);
            },
            error: showError
        });

    }

    function createDesignDoc() {

        var designName = '_design/npcs';

        var designDocument = {
            "views": {
                "value": {
                    "map": "function(doc) { emit( doc.id, {id : doc.npc_id, name : doc.npc_name, value : doc.value, color : doc.color, question : doc.question, answer : doc.answer}); }"
                },
                "question": {
                    "map": "function(doc) { emit(doc.id, {id: doc.npc_id, name : doc.npc_name, question : doc.question}); }"
                },
                "color": {
                    "map": "function(doc) { emit(doc.id, {id: doc.npc_id, name : doc.npc_name, color : doc.color}); }"
                }
            }
        };

        $.ajax({
            type: 'GET',
            url: '/load',
            dataType: 'json',
            data: {
                docName: designName,
                data: designDocument
            },
            success: function(response) {
                console.log(response);
            },
            error: showError
        });
    }


    function displayResponse(response) {

        $("#dbMessage").empty();

        if (!response.isErr) {
            $("#dbMessage").append("<p>" + response.msg + "</p>");
        } else {
            console.log(response.msg);
            $("#dbMessage").append("<p> 500 Internal Error: " + response.msg.message + " </p>");
        }

    }

    function showError(request, ajaxOptions, thrownError) {
        console.log("*****************");
        console.log("Error occurred: = " + ajaxOptions + " " + thrownError);
        console.log(request.status);
        console.log(request.statusText);
        console.log(request.getAllResponseHeaders());
        console.log(request.responseText);
        console.log("*****************");
    }

    return DatabaseControl;
});
