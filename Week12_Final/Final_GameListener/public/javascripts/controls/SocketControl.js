/**
 * SocketControl.js
 */
define(["DataContainer"], function(DataContainer) {
    'use strict';

    var socketGame = null;
    var socketListener = null;

    var dataContainer = null;
    var control = null;

    var flag = false;

    function SocketControl(controlRef) {
        dataContainer = new DataContainer();
        control = controlRef;

        init();
    }

    function init() {
        var socketListener = io.connect('http://localhost:30026');

        var socketTheGame = io.connect('http://localhost:30025');

        socketTheGame.on('socket_is_connected', function(message) {
            $('#connectionGame').html(message);
        });

        socketListener.on('socket_is_connected', function(message) {
            $('#connectionGame').html(message);
        });

        socketTheGame.on('sendGameData', function(gameData) {
            if (!flag) {
                flag = true;
                dataContainer.data = gameData;
                control.reDrawTinyMap();

            }
            dataContainer.data = gameData;
            control.reDrawTinyMap();

        });

        //*************************************
        /* Final Part */
        socketTheGame.on("query", function(data) {
            $.publish("queryDb", data);
        });

        $.subscribe("sendResponse", function(event, eventArgs) {
            console.log(eventArgs);
            socketListener.emit('dbData', eventArgs);

        });


        //*************************************


        socketTheGame.on('chatMessage', function(msg) {
            if (msg.isStarted) {
                $('ul').empty();
            }
            $('#info').prepend($('<li>').text(msg.details));
        });

        $("#chatButton").click(function() {
            socketListener.emit('chatMessage', $('#myInput').val());
            $("#myInput").val("");
        });

    }

    return SocketControl;
});
