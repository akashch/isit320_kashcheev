/**
 * Utilities.js
 */
define([require], function() {
    'use strict';
    var utilities = {
        showDebug: function(data) {
            console.log(data);
        },

        showError: function(request, ajaxOptions, thrownError) {
            showDebug("Error occurred: = " + ajaxOptions + " " + thrownError);
            showDebug(request.status);
            showDebug(request.statusText);
            showDebug(request.getAllResponseHeaders());
            showDebug(request.responseText);
        },
        getName: function(baseName, x, z) {
            return baseName + x + z;
        },
        iterate: function(arr, callback) {
            for (var i = 0; i < arr.length; i++) {
                for (var j = 0; j < arr[i].length; j++) {
                    callback(i, j);
                }
            }
        },
        comparePisiton: function(playerPosition, particles) {
            var xValue = Math.round(playerPosition.x / 20);
            var zValue = Math.round(playerPosition.z / 20);

            if (particles.grid.length > 0 && zValue > -1 && xValue > -1) {
                if (particles.grid[zValue][xValue] !== 0) {
                    xValue = xValue * 20;
                    zValue = zValue * 20;
                    $.publish('updateTinyMap', {
                        message: "NPC was removed from scene",
                        xPosition: xValue,
                        zPosition: zValue
                    });
                    particles.grid[zValue / 20][xValue / 20] = 0; // remove
                    // from grid
                }
            }

        },
        drawTinyMap: function(i, j, colorHex) {
            // console.log(colorHex); //debug
            var c = document.getElementById("myCanvas");
            var context = c.getContext("2d");
            context.fillStyle = colorHex;
            context.fillRect(i * 10, j * 10, 10, 10);
        },
        drawTriangle: function(i, j, radian, colorHex) {
            // Thank you JSFIDDLE: http://jsfiddle.net/sadasant/3sBRh/4/
            var c = document.getElementById("myCanvas");
            var context = c.getContext("2d");
            var v = [
                [0, -7],
                [-7, 0],
                [7, 0]
            ];

            context.save();

            context.translate(10 * i + 5, 10 * j + 5);
            context.rotate(radian);

            context.beginPath();
            context.fillStyle = colorHex;

            /*
             * context.moveTo(10*i,10*j);
             * context.lineTo(10*i,10*j + 10);
             * context.lineTo(10*i + 10, 10*j + 5);
             */

            context.moveTo(v[0][0], v[0][1]);
            context.lineTo(v[1][0], v[1][1]);
            context.lineTo(v[2][0], v[2][1]);

            context.closePath();
            context.stroke();
            context.fill();
            context.restore();
        },
    };

    return utilities;
});
