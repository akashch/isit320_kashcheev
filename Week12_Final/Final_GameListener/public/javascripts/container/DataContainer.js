/**
 * DataContainer.js (Singelton)
 */
define([require], function() {
    'use strict';

    var data = null;

    var _instance = null;

    function DataContainer() {

        if (_instance === null) {
            _instance = this;

            Object.defineProperty(this, "data", {
                get: function() {
                    return data;
                },
                set: function(value) {
                    data = value;
                },
                enumerable: true,
                configurable: true
            });



            //init();
        } else {
            return _instance;
        }

    }

    return DataContainer;
});
