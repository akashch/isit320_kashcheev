/**
 * @author Andrey Kashcheev
 */

var fs = require('fs');
var handlebars = require('handlebars');


handlebars.registerHelper('values', function(context, block) {

    var formattedData = "The formated data: ";
    var ret = formattedData + "<ul>";


    var arr = context.msg.rows;

    for (var i = 0, j = arr.length; i < j; i++) {
        ret = ret + "<li>" + arr[i].value.id + ' : ' + arr[i].value.name + ' : ' + arr[i].value.value + "</li>";
    }

    return ret + "</ul>";
});


handlebars.registerHelper('questions', function(context, block) {

    var formattedData = "The formated data: ";
    var ret = formattedData + "<ul>";


    var arr = context.msg.rows;

    for (var i = 0, j = arr.length; i < j; i++) {
        ret = ret + "<li>" + arr[i].value.id + ' : ' + arr[i].value.name + ' : ' + arr[i].value.question + "</li>";
    }

    return ret + "</ul>";
});

var Templater = (function() {
    'use strict';

    function Templater() {}

    // Please note that we convert to a string.
    var readHtml = function(fileName) {
        return String(fs.readFileSync(fileName));
    };


    Templater.prototype.addNpcs = function(fileName, data) {

        var mainFile = readHtml(fileName);

        var template = handlebars.compile(mainFile);

        var result = template({
            nav: data,
            description: "The templated HTML in this section was created with handlebars.",
            headGuy: fileName
        });

        console.log('Return result');

        return result;
    };



    return Templater;

})();

exports.template = new Templater();
