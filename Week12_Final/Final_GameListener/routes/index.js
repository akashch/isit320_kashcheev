var express = require('express');
var router = express.Router();
var db = require('./couch.js');
var templater = require('./Templater.js');

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', {
        title: 'Game Listeners'
    });
});

router.get('/load', function(request, response) {
    console.log("/load called");

    db.couchDb.insert(request.query.docName, request.query.data, function(
        result) {
        response.send(result);
    });
});

router.get('/create', function(request, response) {
    console.log("/create called");

    db.couchDb.createDb(function(result) {
        response.send(result);
    });
});

router.get('/delete', function(request, response) {
    console.log("/delete called");

    db.couchDb.deleteDb(function(result) {
        response.send(result);
    });
});

var theSwitch = {

    value: function(result, response) {
        response.send(templater.template.addNpcs('Templates/Values.html',
            result));
    },
    question: function(result, response) {
        console.log("Here");
        response.send(templater.template.addNpcs('Templates/Questions.html',
            result));
    }

};

router.get('/viewHB', function(request, response) {
    console.log("/viewHB called");

    db.couchDb.view(request.query.docName, request.query.view,
        function(result) {
            console.log(result);

            if (theSwitch[request.query.view]) {
                theSwitch[request.query.view](result, response);
            } else {
                response.send(result);
            }
        });
});

router.get('/view', function(request, response) {
    console.log("/view called");

    db.couchDb.view(request.query.docName, request.query.view,
        function(result) {
            response.send(result);
        });
});

module.exports = router;
