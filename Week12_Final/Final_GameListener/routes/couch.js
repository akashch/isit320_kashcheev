/**
 * @name: GameListener
 * @author: Andrey Kashcheev
 * @module: couch
 */

var request = require('request');
var nano = require('nano')('http://localhost:5984');

var couchDb = (function() {
    'use strict';

    var _instance = null;
    var nanoDb = null;
    var db = {
        dbUrl: 'http://localhost:5984/',
        dbName: "game_data_kashcheev"
    };

    function couchDb() {
        if (_instance === null) {
            _instance = this;

            Object.defineProperty(this, "createDb", {
                get: function() {
                    return createDb;
                }
            });


            Object.defineProperty(this, "deleteDb", {
                get: function() {
                    return deleteDb;
                }
            });

            Object.defineProperty(this, "insert", {
                get: function() {
                    return insert;
                }
            });

            Object.defineProperty(this, "read", {
                get: function() {
                    return read;
                }
            });

            Object.defineProperty(this, "view", {
                get: function() {
                    return view;
                }
            });

            init();
        } else {
            return _instance;
        }
    }

    function init() {
        createDb(function(data) {
            console.log(data);
        });
        nanoDb = nano.db.use(db.dbName);

    }

    function createDb(callback) {
        console.log("couchdb - createDb method"); // debug

        request.head(db.dbUrl + "" + db.dbName,
            function(error, response, body) {
                if (response.statusCode !== 200) {
                    request.put(db.dbUrl + "" + db.dbName, function(err,
                        response, body) {
                        callback({
                            isErr: false,
                            status: response.statusCode,
                            msg: db.dbName + " database has been created"
                        });
                    });
                } else {
                    callback({
                        isErr: true,
                        status: response.statusCode,
                        msg: {
                            message: "Database already exists"
                        }
                    });
                }
            });
    }

    function deleteDb(callback) {
        console.log("couchdb - deleteDb method"); // debug

        nano.db.destroy(db.dbName, function(err, body) {
            if (!err) {
                callback({
                    isErr: false,
                    status: 200,
                    msg: db.dbName + " has been deleted"
                });
            } else {
                callback({
                    isErr: true,
                    status: 500,
                    msg: err
                });
            }
        });

    }

    function insert(docName, docData, callback) {
        console.log("couchdb - insert method"); // debug

        nanoDb.insert(docData, docName, function(err, body) {
            if (!err) {
                console.log(body);
                callback({
                    isErr: false,
                    status: 201,
                    msg: "success"
                });
            } else {
                console.log(err);
                callback({
                    isErr: true,
                    status: 500,
                    msg: err
                });
            }
        });

    }

    function read(docName, callback) {
        console.log("Couch - Read Called");

        nanoDb.get(docName, {
            revs_info: true
        }, function(err, body) {
            if (!err) {
                console.log(body);
                callback({
                    isErr: false,
                    status: 200,
                    msg: body
                });
            } else {
                callback({
                    isErr: true,
                    status: 500,
                    msg: err
                });
            }
        });

    }

    function view(designDoc, view, callback) {
        console.log("Couch - View Called");
        console.log(view);
        nanoDb.view(designDoc, view, function(err, body) {
            if (!err) {
                console.log(body);
                callback({
                    isErr: false,
                    status: 200,
                    msg: body
                });
            } else {
                callback({
                    isErr: true,
                    status: 500,
                    msg: err
                });
            }
        });

    }

    return couchDb;

}());

exports.couchDb = new couchDb();
