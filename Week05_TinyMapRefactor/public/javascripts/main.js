/**
 * main.js
 */
require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/lib/jquery',
        "TinyPubSub" : "javascripts/lib/TinyPubSub",
        "Three" : 'javascripts/lib/three',
        "MTLLoader" : 'javascripts/lib/MTLLoader',
        "OBJMTLLoader" : 'javascripts/lib/OBJMTLLoader',
        "ColladaLoader" : 'javascripts/lib/ColladaLoader',
        "Core" : "javascripts/Core",
        "Control" : 'javascripts/Control',
        "SocketControl" : "javascripts/SocketControl",
        "Floors" : 'javascripts/Floors',
        "PointerLockControls" : "javascripts/lib/PointerLockControls",
        "PointerLockSetup":"javascripts/PointerLockSetup",
        "utilities":"javascripts/utilities",
        "Particles" : "javascripts/Particles",
        "Buildings" : "javascripts/Buildings",
        "Shapes" : "javascripts/Shapes"
    },
    shim : {
        'Three' : {
            exports : 'Three'
        },
        'PointerLockControls' : {
            exports : 'PointerLockControls'
        },
        'MaterialLoader' : {
            exports : 'MTLLoader'
        },
        'OBJMTLLoader' : {
            exports : 'OBJMTLLoader'
        },
        'ColladaLoader' : {
            exports : 'ColladaLoader'
        },
    		"TinyPubSub" : {
    			deps : [ "jquery" ],
    			exports : "TinyPubSub"
    	}
        
    }
});

require([ 'jquery', 'TinyPubSub', 'Three', 'Core', 'Control', 'SocketControl', 'Floors', 'PointerLockSetup', 'utilities', 'Particles', 'Buildings', 'Shapes', 'utilities'], 
		function(jq, TinyPubSub, Three, Core, Control, SocketControl, Floors, PointerLockSetup, Particles, Buildings, Shapes, util) {

    'use strict';

    $(document).ready(function() {
        var control = new Control();
    });

});

