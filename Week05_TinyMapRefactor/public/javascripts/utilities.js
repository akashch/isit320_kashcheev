/**
 * Utilities.js
 */
define([ 'Core' ], function(Core) {
	'use strict';
	var utilities = {
		showDebug : function(data) {
			console.log(data);
		},

		showError : function(request, ajaxOptions, thrownError) {
			showDebug("Error occurred: = " + ajaxOptions + " " + thrownError);
			showDebug(request.status);
			showDebug(request.statusText);
			showDebug(request.getAllResponseHeaders());
			showDebug(request.responseText);
		},
		getName : function(baseName, x, z) {
	        return baseName + x + z;
		},
		iterate : function(arr, callback){
			for(var i=0; i<arr.length; i++){
				for(var j=0; j<arr[i].length;j++){
					callback(i, j);
				}
			}
		},
		comparePisiton : function(playerPosition){
			var core = new Core();
			var xValue = Math.round(playerPosition.x/20);
			var zValue = Math.round(playerPosition.z/20);
			
			if(core.grid.length > 0 && zValue > -1 && xValue > -1){
				if(core.grid[zValue][xValue] !== 0){
					xValue = xValue*20;
					zValue = zValue*20;
					$.publish('updateTinyMap', {
						message : "NPC was removed from scene",
						xPosition : xValue,
						zPosition : zValue
					});
					core.grid[zValue/20][xValue/20] = 0; //remove from grid
				}
			}
			
		},
		drawTinyMap : function(i, j, colorHex){
			//console.log(colorHex); //debug
			var c = document.getElementById("myCanvas");
			var context = c.getContext("2d");
			context.fillStyle = colorHex;
			context.fillRect(i*10, j*10, 10, 10);
		}
	}
	
	return utilities;
});
