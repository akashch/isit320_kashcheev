/**
 * Particles.js
 */
define([ "Core", "Shapes", "utilities" ],
		function(Core, Shapes, utilities) {

			var particles = [];
			var npcs = [];
			var that = {};
			var shapes;
			var core;

			var grid = [];

			function Particles() {
				// instantiated
				shapes = new Shapes();
				core = new Core();
				
				Object.defineProperty(this, "grid", {
					get : function() {
						return grid;
					},
					set : function(x, z, value) {
						grid[x][z] = value;
					},
					enumerable : true,
					configurable : true
				});
			}

			function showParticles( x, y) {
				var geometry = new THREE.IcosahedronGeometry(10, 2);
				var material = new THREE.PointCloudMaterial({
					color : 0x00AA00,
					size : 0.2
				});
				var particleSystem = new THREE.PointCloud(geometry, material);
				particleSystem.position.set(x, 10, y);
				particleSystem.name = utilities.getName("PCloud", x, y);
				core.scene.add(particleSystem);
				particles.push(particleSystem);
			}

			Particles.prototype.rotateParticlesAroundWorldAxis = function(
					npcIndex, axis, radians, npc) {
				if (npcs.length > 0) {
					for (var i = 0; i < npcs.length; i++) {
						var object;
						if (npc === true) {
							object = npcs[i];
						} else {
							object = particles[i];
						}

						that.rotWorldMatrix = new THREE.Matrix4();
						that.rotWorldMatrix.makeRotationAxis(axis.normalize(),
								radians);

						that.rotWorldMatrix.multiply(object.matrix); // pre-multiply

						object.matrix = that.rotWorldMatrix;

						object.rotation.setFromRotationMatrix(object.matrix);
					}
				}
			};

			Particles.prototype.initNpc = function(fileName) {
				$.ajax({
					url : fileName,
					cache : false,
					type : "GET",
					dataType : "json",
					success : function(gridData) {
						grid = gridData; //Need to be refactored
						core.grid = gridData;
						utilities.showDebug('Opening file: ' + fileName);

						utilities.iterate(gridData, function(i, j) {
							var npcValue = gridData[i][j];
							if (npcValue !== 0) {
								shapes.addStarObject(npcs, false, j * size, i * size);
								showParticles(j * size, i * size);
							}
						});
					},

					error : utilities.showError
				});
			};

			return Particles;
		});