/**
 * 
 * Core.js (Singelton pattern)
 * 
 */

define([require], function() {
	'use strict';

	var _instance = null;

	var scene = null;
	var renderer = null;
	var camera = null;
	var raycaster = null;
	
	//TO DO 
	var grid = [];
	var npcs = [];


	function Core() {

		if (_instance === null) {
			_instance = this;
			
			Object.defineProperty(this, "scene", {
				get : function() {
					return scene;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "camera", {
				get : function() {
					return camera;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "renderer", {
				get : function() {
					return renderer;
				},
				enumerable : true,
				configurable : true
			});
			
			Object.defineProperty(this, "raycaster", {
				get : function() {
					return raycaster;
				},
				enumerable : true,
				configurable : true
			});
			
			Object.defineProperty(this, "grid", {
				get : function() {
					return grid;
				},
				set : function(arr) {
					grid = arr;
				},
				enumerable : true,
				configurable : true
			});
			
			Object.defineProperty(this, "npcs", {
				get : function() {
					return npcs;
				},
				set : function(arr) {
					npcs = arr;
				},
				enumerable : true,
				configurable : true
			});

			init();
		} else {
			return _instance;
		}
	}

	function init() {
		var screenWidth = window.innerWidth / window.innerHeight;
		camera = new THREE.PerspectiveCamera(75, screenWidth, 1, 1000);

		scene = new THREE.Scene();
		scene.fog = new THREE.Fog(0xffffff, 0, 750);
		raycaster = new THREE.Raycaster(new THREE.Vector3(), new THREE.Vector3(
				0, -1, 0), 0, 10);

		renderer = new THREE.WebGLRenderer({
			antialias : true
		});

		renderer.setSize(window.innerWidth, window.innerHeight);
		document.body.appendChild(renderer.domElement);
		$.subscribe("updateTinyMap", function(event, eventArgs){
			updateScene(eventArgs.xPosition, eventArgs.zPosition);
		});

	}
	
	function updateScene(xValue, zValue){
		scene.remove(scene.getObjectByName(getName("Star", xValue, zValue)));
		scene.remove(scene.getObjectByName(getName("PCloud", xValue, zValue)));
	}
	
	
	//This method needs to be created here because when I define utilities, 
	//it makes Core is undefined under utilities.
	function getName(baseName, x, z) {
        return baseName + x + z;
	}
	return Core;
});