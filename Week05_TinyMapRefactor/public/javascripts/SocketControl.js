/**
 * SocketControl.js (Singelton)
 */
define([require], function() {
	'use strict';
	
	var socketGame = null;
	var socketListener = null;
	
	var _instance = null;
	
	function SocketControl(){
		
		if (_instance === null) {
			_instance = this;
			

			init();
		} else {
			return _instance;
		}
		
	}
	
	function init() {
		socketGame = io.connect('http://localhost:30025');
		
		socketListener = io.connect('http://localhost:30026');

		socketListener.on('socket_is_connected', function(message) {
			$('#connectionListener').html(message);
		});
		
		socketGame.on('socket_is_connected', function(message) {
			$('#connectionGame').html(message);
		});


	}
	
	SocketControl.prototype.sendData2Listener = function(position, core, particles){
		socketGame.emit("sendData2Listener", {
			"position" : position,
			"grid" : core.grid,
			"npcs" : particles.grid
		});
	};
	
	return SocketControl;
});