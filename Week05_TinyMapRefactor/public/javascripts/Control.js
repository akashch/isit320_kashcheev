/**
 * Control.js
 */
define([ "Core", "SocketControl", "Floors", "PointerLockSetup", "Buildings", "Particles",
		"utilities" ], function(Core, SocketControl, Floors, PointerLockSetup, Buildings,
		Particles, utilities) {

	var core = null;
	var controls = null;
	var animateNpc = false;

	var particles = null;
	var buildings = null;
	var position = null;

	var xTemp = null;
	var zTemp = null;
	
	var socketCtrl = null;


	var cameraPosition = {
		x : 0,
		y : 0,
		z : 0
	};

	function Control() {
		init();
	}

	function init() {

		$.getJSON("GameData.json", function(data) {

			// setting GameData provided in csv
			size = parseInt(data[0].Value01);
			cameraPosition.x = data[3].Value01;
			cameraPosition.y = data[3].Value02;
			cameraPosition.z = data[3].Value03;
			numberOfCubes = parseInt(data[2].Value01);
			cubesImage = data[1].Value01;
			// ********************************

			core = new Core();
			particles = new Particles();// ??
			particles.initNpc("Npc000.json");

			buildings = new Buildings();
			buildings.addCubes(false);

			floors = new Floors(data);
			floors.drawFloor();

			doPointerLock();
			
			socketCtrl = new SocketControl();
			
			animate();

			window.addEventListener('resize', onWindowResize, false);
			$("#gameData").load("GameData.html");
			
			

		});

	}

	

	function doPointerLock() {
		controls = new THREE.PointerLockControls(core.camera);

		var yawObject = controls.getObject();
		core.scene.add(yawObject);

		// Move camera to the 1, 1 position
		yawObject.position.x = size;
		yawObject.position.z = size;

		var ps = new PointerLockSetup(controls);
		

	}

	function animate() {
		
		var controlObject = controls.getObject();
		position = controlObject.position;

		
		socketCtrl.sendData2Listener(position, buildings, particles);
		reDrawTinyMap();

		requestAnimationFrame(animate);
		var xAxis = new THREE.Vector3(1, 0, 0);
		particles.rotateParticlesAroundWorldAxis(0, xAxis, Math.PI / 180,
				animateNpc);
		animateNpc = !animateNpc;

		controls.isOnObject(false);

		buildings.collisionDetection(position, controls);

		// Move the camera
		controls.update();

		// DEBUG DATA
		$("#cameraPosition").html(
				"X : " + Math.round(position.x) + " <br>Y : "
						+ Math.round(position.y) + "<br>Z : "
						+ Math.round(position.z));
		core.renderer.render(core.scene, core.camera);

	}

	function reDrawTinyMap() {

		// MAP
		utilities.comparePisiton(position);

		utilities.iterate(buildings.grid, function(i, j) {
			if (buildings.grid[i][j] === 1) {
				utilities.drawTinyMap(i, j, "#FF0000");
			} else {
				utilities.drawTinyMap(i, j, "#0000FF");
			}
		});

		// Player
		utilities.drawTinyMap(Math.round(position.x) / size, Math
				.round(position.z)
				/ size, "#00FFFF");

		// NPCS
		utilities.iterate(particles.grid, function(i, j) {
			if (particles.grid[j][i] !== 0) {
				utilities.drawTinyMap(i, j, "#FFFF00");
			}
		});
	}

	function onWindowResize() {
		core.camera.aspect = window.innerWidth / window.innerHeight;
		core.camera.updateProjectionMatrix();
		core.renderer.setSize(window.innerWidth, window.innerHeight);
	}

	return Control;
});