/**
w  * Buildings.js
 */
define([ "Core", "utilities" ], function(Core, utilities) {

	var size = 20;
	var scene = null;
	var cubes = [];
	var cube = null;
	var grid = [];
	
	var core = null;
	
	var isCollided = false;

	function Buildings() {
		core = new Core();
		
		Object.defineProperty(this, "grid", {
			get : function() {
				return grid;
			},
			enumerable : true,
			configurable : true
		});
	};

	Buildings.prototype.addCubes = function(wireFrame) {
		$.getJSON("MazeBuilder.json", function(data) {
			utilities.iterate(data, function(i, j){
				if (data[i][j] === 1) {
					grid = data;
					addCube( wireFrame, size * i, 20 * j, 20 * j / 100);
				}
			});
			addLights();
		});
	}

	Buildings.prototype.collisionDetection = function(position,
			controls) {

		// Collision detection
		core.raycaster.ray.origin.copy(position);
		// raycaster.ray.origin.y -= 10;
		var dir = controls.getDirection(new THREE.Vector3(0, 0, 0)).clone();
		core.raycaster.ray.direction.copy(dir);

		var intersections = core.raycaster.intersectObjects(cubes);

		// If we hit something (a wall) then stop moving in
		// that direction
		if (intersections.length > 0 && intersections[0].distance <= 215) {
			console.log(intersections.length);
			controls.isOnObject(true);
			isCollided = true;
		} else
			isCollided = false;
		
		$("#collision").html("isCollided : " + isCollided);
	}

	function addCube(wireFrame, x, z, dz) {
		var geometry = new THREE.BoxGeometry(size, size, size);
		var material = new THREE.MeshLambertMaterial({
			map : THREE.ImageUtils.loadTexture('images/crate.jpg')
		});
		var cube = new THREE.Mesh(geometry, material);
		if (typeof dz === "undefined") {
			cube.position.set(x, 10, z);
		} else {
			cube.position.set(x, 10, z + dz);
		}
		core.scene.add(cube);

		cubes.push(cube);

		return cube;
	}

	function addLights() {
		var light = new THREE.DirectionalLight(0xffffff, 1.5);
		light.position.set(1, 1, 1);
		core.scene.add(light);
		light = new THREE.DirectionalLight(0xffffff, 0.75);
		light.position.set(-1, -0.5, -1);
		core.scene.add(light);
	}

	function addSphere(wireFrame, x, y) {
		var geometry = new THREE.SphereGeometry(.5, 25, 25);
		var material = new THREE.MeshNormalMaterial({
			color : 0x00ffff,
			wireframe : wireFrame
		});

		var sphere = new THREE.Mesh(geometry, material);
		sphere.overdraw = true;
		sphere.position.set(x, 1.6, y);
		core.scene.add(sphere);

		return sphere;
	}

	return Buildings;

});