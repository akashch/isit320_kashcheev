/**
 * Control.js
 */
define([ "Floors", "PointerLockSetup"], function(Floors, PointerLockSetup) {

	var scene = null;
	var renderer = null;
	var camera = null;
    var gameData = null;
    
    
	var cubes = [];
     var cubesImage = null;
	var cube = null;
	var size = null;
	
	var floors = null;
	var keyMove = {
		moveForward : false,
		moveBackward : false,
		moveLeft : false,
		moveRight : false,
		jump:false
	};
    
    var numberOfCubes = null;

	var cameraPosition = {
		x : null,
		y : null,
		z : null
	};

	function Control() {
		
	    init();
	}
	
	function init() {
		$.getJSON("GameData.json",function(data){

		    
		    //setting GameData provided in csv
		    size = parseInt(data[0].Value01);
		    cameraPosition.x = data[3].Value01;
		    cameraPosition.y = data[3].Value02;
		    cameraPosition.z = data[3].Value03;
		    numberOfCubes = parseInt(data[2].Value01);
		    cubesImage = data[1].Value01;
		    //********************************
		    
		    var screenWidth = window.innerWidth / window.innerHeight;
		    camera = new THREE.PerspectiveCamera(75, screenWidth, 1, 1000);

		   scene = new THREE.Scene();
		   scene.fog = new THREE.Fog(0xffffff, 0, 750);


		   addCubes(scene, camera, false);
		   doPointerLock();
		   addLights();
		    
		   var floors = new Floors(data);
		   floors.drawFloor(scene);

		   raycaster = new THREE.Raycaster(new THREE.Vector3(), 
			  new THREE.Vector3(0, -1, 0), 0, 10);

		   renderer = new THREE.WebGLRenderer({ antialias : true });

		   renderer.setSize(window.innerWidth, window.innerHeight);
		   document.body.appendChild(renderer.domElement);

		   window.addEventListener('resize', onWindowResize, false);

		   $("#gameData").load("GameData.html");
		   animate();
		   
		});
	}
    
	function doPointerLock() {
	    controls = new THREE.PointerLockControls(camera);
	    var yawObject = controls.getObject();
	    scene.add(yawObject);
	    
	    // Move camera to the 1, 1 position
	    yawObject.position.x = size;
	    yawObject.position.z = size;

	    var ps = new PointerLockSetup(controls);
	}
	
	function animate() {

	    requestAnimationFrame(animate);

	    var xAxis = new THREE.Vector3(1, 0, 0);

	    controls.isOnObject(false);

	    var controlObject = controls.getObject();
	    var position = controlObject.position;
	                
	    // drawText(controlObject, position);

	    collisionDetection(position);

	    // Move the camera
	    controls.update();

	    renderer.render(scene, camera);
	}
	
	function collisionDetection(position) {
	    // Collision detection
	    raycaster.ray.origin.copy(position);
	    // raycaster.ray.origin.y -= 10;
	    var dir = controls.getDirection(new THREE.Vector3(0, 0, 0)).clone();
	    raycaster.ray.direction.copy(dir);

	    var intersections = raycaster.intersectObjects(cubes);

	    // If we hit something (a wall) then stop moving in
	    // that direction
	    if (intersections.length > 0 && intersections[0].distance <= 215) {
	        console.log(intersections.length);
	        controls.isOnObject(true);
	    }
	}
	
	var onKeyDown = function(event) {

		switch (event.keyCode) {

		case 38: // up
		case 87: // w
			keyMove.moveForward = true;
			break;

		case 37: // left
		case 65: // a
			keyMove.moveLeft = true;
			break;

		case 40: // down
		case 83: // s
			keyMove.moveBackward = true;
			break;

		case 39: // right
		case 68: // d
			keyMove.moveRight = true;
			break;
			
		case 32: //space
			keyMove.jump = true;
			break;
		}
	};

	var onKeyUp = function(event) {
		
		switch (event.keyCode) {

		case 38: // up
		case 87: // w
			keyMove.moveForward = false;
			break;

		case 37: // left
		case 65: // a
			keyMove.moveLeft = false;
			break;

		case 40: // down
		case 83: // s
			keyMove.moveBackward = false;
			break;

		case 39: // right
		case 68: // d
			keyMove.moveRight = false;
			break;  
			
		case 32: //space
			keyMove.jump = false;
			break;
		}
	};
	
	function onWindowResize() {
	    camera.aspect = window.innerWidth / window.innerHeight;
	    camera.updateProjectionMatrix();
	    renderer.setSize(window.innerWidth, window.innerHeight);
	}
	
	function addSphere(sne, camera, wireFrame, x, y) {
	    var geometry = new THREE.SphereGeometry(0.5, 25, 25);
	    var material = new THREE.MeshNormalMaterial({
	    color: 0x00ffff,
	        wireframe: wireFrame
	    }); 
	        
	    var sphere = new THREE.Mesh(geometry, material);
	    sphere.overdraw = true;
	    sphere.position.set(x, 1.6, y);
	    scene.add(sphere);

	        return sphere;
	}
	
	function addLights() {
	    var light = new THREE.DirectionalLight(0xffffff, 1.5);
	    light.position.set(1, 1, 1);
	    scene.add(light);
	    light = new THREE.DirectionalLight(0xffffff, 0.75);
	    light.position.set(-1, -0.5, -1);
	    scene.add(light);
	}
	
	function addCubes(scene, camera, wireFrame) {
	    for (var i = 0; i < numberOfCubes; i++) {
	    	addCube(scene, camera, wireFrame, size, size*i, size*i/2);
	    	addCube(scene, camera, wireFrame, -size, size*i, size*i/2);
	    }
	}
	
	function addCube(scene, camera, wireFrame, x, z, dz) {
		var geometry = new THREE.BoxGeometry(size, size, size);
		var material = new THREE.MeshLambertMaterial({ map : THREE.ImageUtils.loadTexture('images/' + cubesImage) });
		var cube = new THREE.Mesh(geometry, material);
		if(typeof dz === "undefined"){
			cube.position.set(x, size/2, z);
		}else{
			cube.position.set(x, size/2, z+dz);
		}
		scene.add(cube);
		
		cubes.push(cube);
		
		return cube;
	}
	

	return Control;
});