/**
 * main.js
 */
require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/jquery',
        "Three" : 'javascripts/three',
        "Control" : 'javascripts/Control',
        "Floors" : 'javascripts/Floors',
        "PointerLockControls" : "javascripts/PointerLockControls",
        "PointerLockSetup":"javascripts/PointerLockSetup"
    },
    shim : {
        'Three' : {
            exports : 'Three'
        },
        'PointerLockControls' : {
            exports : 'PointerLockControls'
        }
    }
});

require([ 'jquery', 'Three', 'Control', 'Floors','PointerLockSetup'], function(jq, Three, Control, Floors, PointerLockSetup) {

    'use strict';

    $(document).ready(function() {
        var control = new Control();
    });

});

