/**
 * Control.js
 */
define([ "Floors" ], function(Floors) {

	var scene = null;
	var renderer = null;
	var camera = null;
	
	var cube = null;
	var floors = null;
	var keyMove = {
		moveForward : false,
		moveBackward : false,
		moveLeft : false,
		moveRight : false,
		jump:false
	};

	var cameraPosition = {
		x : 1,
		y : 0,
		z : 8
	}

	function Control() {
		
		window.addEventListener('resize', onWindowResize, false);
		document.addEventListener('keydown', onKeyDown, false);
		document.addEventListener('keyup', onKeyUp, false);
		
		scene = new THREE.Scene();
		var width = window.innerWidth / window.innerHeight;
		
		//camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
		camera = new THREE.PerspectiveCamera(100, width, 0.1, 1000);
		
		renderer = new THREE.WebGLRenderer({
			antialias : true
		});
		
		renderer.setSize(window.innerWidth, window.innerHeight);
		document.body.appendChild(renderer.domElement);
		
		cube = addCube(scene, camera, false, 1, -3);
		addSphere(scene, camera, false, 1, -3)
		addCubes(scene, camera, false);
		floors = new Floors();
		floors.drawFloor(scene);
		addLights();
		
		camera.position.z = 10;
		camera.position.x = 2;
		camera.position.y = 0;
		render();
	}
	
	function render() {
		
	    if (keyMove.moveLeft) {
	        cameraPosition.x -= .5;
	    }else if (keyMove.moveRight) {
	        cameraPosition.x += .5;
	    }else if (keyMove.moveForward) {
	        cameraPosition.z -= .5;
	    }else if (keyMove.moveBackward) {
	        cameraPosition.z += .5;
	    }else if(keyMove.jump){
	    	cameraPosition.y += .1;
	    	setTimeout(function(){
	    		cameraPosition.y = 0;
	    	}, 100);
	    	
	    }

		requestAnimationFrame(render);
		cube.rotation.x += 0.01;
		cube.rotation.y += 0.01;
		renderer.render(scene, camera);
		camera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
	}

	var onKeyDown = function(event) {

		switch (event.keyCode) {

		case 38: // up
		case 87: // w
			keyMove.moveForward = true;
			break;

		case 37: // left
		case 65: // a
			keyMove.moveLeft = true;
			break;

		case 40: // down
		case 83: // s
			keyMove.moveBackward = true;
			break;

		case 39: // right
		case 68: // d
			keyMove.moveRight = true;
			break;
			
		case 32: //space
			keyMove.jump = true;
			break;
		}
	};

	var onKeyUp = function(event) {
		
		switch (event.keyCode) {

		case 38: // up
		case 87: // w
			keyMove.moveForward = false;
			break;

		case 37: // left
		case 65: // a
			keyMove.moveLeft = false;
			break;

		case 40: // down
		case 83: // s
			keyMove.moveBackward = false;
			break;

		case 39: // right
		case 68: // d
			keyMove.moveRight = false;
			break;  
			
		case 32: //space
			keyMove.jump = false;
			break;
		}
	};
	
	function onWindowResize() {
	    camera.aspect = window.innerWidth / window.innerHeight;
	    camera.updateProjectionMatrix();
	    renderer.setSize(window.innerWidth, window.innerHeight);
	}
	
	function addSphere(sne, camera, wireFrame, x, y) {
	    var geometry = new THREE.SphereGeometry(.5, 25, 25);
	    var material = new THREE.MeshNormalMaterial({
	    color: 0x00ffff,
	        wireframe: wireFrame
	    }); 
	        
	    var sphere = new THREE.Mesh(geometry, material);
	    sphere.overdraw = true;
	    sphere.position.set(x, 1.6, y);
	    scene.add(sphere);

	        return sphere;
	}
	
	function addLights() {
	    var light = new THREE.DirectionalLight(0xffffff, 1.5);
	    light.position.set(1, 1, 1);
	    scene.add(light);
	    light = new THREE.DirectionalLight(0xffffff, 0.75);
	    light.position.set(-1, -0.5, -1);
	    scene.add(light);
	}
	
	function addCubes(scene, camera, wireFrame) {
	    for (var i = 0; i < 6; i++) {
	    	addCube(scene, camera, wireFrame, 3, i, i/10);
	    	addCube(scene, camera, wireFrame, -1, i, i/10);
	    }
	}
	
	function addCube(scene, camera, wireFrame, x, z, dz) {
		var geometry = new THREE.BoxGeometry(1, 1, 1);
		var material = new THREE.MeshLambertMaterial({ map : THREE.ImageUtils.loadTexture('images/crate.jpg') });
		var cube = new THREE.Mesh(geometry, material);
		if(typeof dz === "undefined"){
			cube.position.set(x, .3, z);
		}else{
			cube.position.set(x, .3, z+dz);
		}
		scene.add(cube);

		return cube;
	}
	

	return Control;
});