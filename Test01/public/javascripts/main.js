/**
 * main.js
 */
require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/jquery',
        "Three" : 'javascripts/three',
        "Control" : 'javascripts/Control',
        "Floors" : 'javascripts/Floors',
        "Ceiling":"javascripts/Ceiling"
    },
    shim : {
        'Three' : {
            exports : 'Three'
        }
    }
});

require([ 'jquery', 'Three', 'Control', 'Floors','Ceiling' ], function(jq, Three, Control, Floors, Ceiling) {

    'use strict';

    $(document).ready(function() {
        var control = new Control();
    });

});

