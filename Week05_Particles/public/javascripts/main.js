/**
 * main.js
 */
require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/lib/jquery',
        "Three" : 'javascripts/lib/three',
        "MTLLoader" : 'javascripts/lib/MTLLoader',
        "OBJMTLLoader" : 'javascripts/lib/OBJMTLLoader',
        "ColladaLoader" : 'javascripts/lib/ColladaLoader',
        "Control" : 'javascripts/Control',
        "Floors" : 'javascripts/Floors',
        "PointerLockControls" : "javascripts/lib/PointerLockControls",
        "PointerLockSetup":"javascripts/PointerLockSetup",
        "utilities":"javascripts/utilities",
        "Particles" : "javascripts/Particles",
        "Buildings" : "javascripts/Buildings",
        "Shapes" : "javascripts/Shapes"
    },
    shim : {
        'Three' : {
            exports : 'Three'
        },
        'PointerLockControls' : {
            exports : 'PointerLockControls'
        },
        'MaterialLoader' : {
            exports : 'MTLLoader'
        },
        'OBJMTLLoader' : {
            exports : 'OBJMTLLoader'
        },
        'ColladaLoader' : {
            exports : 'ColladaLoader'
        }
        
    }
});

require([ 'jquery', 'Three', 'Control', 'Floors', 'PointerLockSetup', 'utilities', 'Particles', 'Buildings', 'Shapes', 'utilities'], 
		function(jq, Three, Control, Floors, PointerLockSetup, Particles, Buildings, Shapes, util) {

    'use strict';

    $(document).ready(function() {
        var control = new Control(util);
    });

});

