/**
w  * Buildings.js
 */
define([ require ], function() {

	var size = 20;
	var scene = null;
	var cubes = [];
	var cube = null;
	var gameData = null;
	var util = null;
	
	var isCollided = false;

	function Buildings(sceneRef, gameDataRef, utilRef) {
		scene = sceneRef;
		gameData = gameDataRef;
		util = utilRef;
	};

	Buildings.prototype.addCubes = function(scene, camera, wireFrame) {
		$.getJSON("MazeBuilder.json", function(data) {
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < data[i].length; j++) {
					if (data[i][j] === 1) {
						addCube(scene, camera, wireFrame, size * i, 20 * j,
								20 * j / 100);
					}
				}
			}
			addLights();
		});
	}

	Buildings.prototype.collisionDetection = function(position, raycaster,
			controls) {

		// Collision detection
		raycaster.ray.origin.copy(position);
		// raycaster.ray.origin.y -= 10;
		var dir = controls.getDirection(new THREE.Vector3(0, 0, 0)).clone();
		raycaster.ray.direction.copy(dir);

		var intersections = raycaster.intersectObjects(cubes);

		// If we hit something (a wall) then stop moving in
		// that direction
		if (intersections.length > 0 && intersections[0].distance <= 215) {
			console.log(intersections.length);
			controls.isOnObject(true);
			isCollided = true;
		} else
			isCollided = false;
		
		$("#collision").html("isCollided : " + isCollided);
	}

	function addCube(scene, camera, wireFrame, x, z, dz) {
		var geometry = new THREE.BoxGeometry(size, size, size);
		var material = new THREE.MeshLambertMaterial({
			map : THREE.ImageUtils.loadTexture('images/crate.jpg')
		});
		var cube = new THREE.Mesh(geometry, material);
		if (typeof dz === "undefined") {
			cube.position.set(x, 10, z);
		} else {
			cube.position.set(x, 10, z + dz);
		}
		scene.add(cube);

		cubes.push(cube);

		return cube;
	}

	function addLights() {
		var light = new THREE.DirectionalLight(0xffffff, 1.5);
		light.position.set(1, 1, 1);
		scene.add(light);
		light = new THREE.DirectionalLight(0xffffff, 0.75);
		light.position.set(-1, -0.5, -1);
		scene.add(light);
	}

	function addSphere(sne, camera, wireFrame, x, y) {
		var geometry = new THREE.SphereGeometry(.5, 25, 25);
		var material = new THREE.MeshNormalMaterial({
			color : 0x00ffff,
			wireframe : wireFrame
		});

		var sphere = new THREE.Mesh(geometry, material);
		sphere.overdraw = true;
		sphere.position.set(x, 1.6, y);
		scene.add(sphere);

		return sphere;
	}

	return Buildings;

});