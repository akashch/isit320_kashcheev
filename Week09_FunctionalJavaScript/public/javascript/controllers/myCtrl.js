/**
 * Controller
 */

var mainModule = angular.module("mainModule", ['ngRoute']);


mainModule.config(function($routeProvider) {
	"use strict";
	$routeProvider.when("/", {
		controller : "MyCtrl",
		templateUrl : "partials/view01.html"
	})
	$routeProvider.when("/view01", {
		controller : "MyCtrl",
		templateUrl : "partials/view01.html"
	})
	$routeProvider.when("/view02", {
		controller : "MyCtrl",
		templateUrl : "partials/view02.html"
	}).otherwise({
		redirectTo : '/'
	});
});


mainModule.controller("MyCtrl", function($scope, myFactory) {
	"use strict";
	
	myFactory.geNames(function(names){
		$scope.names = names;
	});
	
	$scope.startWith = function(){
		$scope.names = myFactory.startWith($scope.letter);
		$scope.letter = "";
	};
	
	$scope.contains = function(){
		$scope.names = myFactory.contains($scope.letter);
		$scope.letter = "";
	};
	
	
});
