/**
 * SocketStuff.js
 */
module.exports = {

    init : function(server) {
        console.log("init called");
        
        var io = require('socket.io').listen(server);
        
        io.sockets.on('connection', function(socket) {
            socket.emit('socket_is_connected', 'Listener is connected to the server!');
            
            socket.on('disconnect', function() {
                console.log('disconnected event');
            });
        });
    },

};