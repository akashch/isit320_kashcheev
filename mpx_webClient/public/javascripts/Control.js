/**
 * @Contorls.js 
 * @author: Andrey Kashcheev
 */

define([ "DataContainer" ], function( DataContainer ) {
	'use strict';

	var dataContainer = null;
	
	function Control() {
		console.log("Control called"); // debug
		init();
	}

	var init = function() {
		$("#signIn").click(signIn);
		
	};

	// Requesting token
	var signIn = function() {
		dataContainer = new DataContainer($("#username").val());
		
		$.ajax({
			type : 'GET',
			url : '/identity/signIn',
			dataType : 'json',
			data : {
				userName : $("#username").val(),
				password : $("#password").val()
			},
			success : function(data) {
				console.log(data);//debug
				$.ajax({
					type : 'GET',
					url : '/access/loginUser',
					dataType : 'json',
					data : {
						userName : dataContainer.userName
					},
					success : function(data) {
						console.log(data);//debug
						
						
					}
				});
				
			}
		});
		
	};
	
	

	return Control;
});