/**
 * main.js
 */
require.config({
	baseUrl : '.',
	paths : {
		"jquery" : 'javascripts/lib/jquery',
		"DataContainer" : "javascripts/DataContainer",
		"Control" : 'javascripts/Control'
		
	},
	shim : {
		"TinyPubSub" : {
			deps : [ "jquery" ],
			exports : "TinyPubSub"
		}
	}
});

require([ 'jquery', 'Control'], function(jq, Control) {
	'use strict';
		$(document).ready(function() {
			var control = new Control();
		});
});
