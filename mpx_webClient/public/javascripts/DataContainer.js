/**
 * New node file
 */

define([ require ], function() {
	'use strict';
	
	var _instance = null;
	var userName = null;
	
	
	function DataContainer(data){
		if(_instance === null){
			_instance = this;
			
			Object.defineProperty(this, "userName", {
				get : function() {
					return userName;
				}
			});
			
			init(data);
			
		}else{
			return _instance;
		}
		
	}
	
	function init(data){
		userName = data;
		
	}
	return DataContainer;
});