/**
 * @name mpx WebClient
 * @author: Andrey Kashcheev
 * @module: access
 */

var express = require('express');
var router = express.Router();
var http = require("https");
var db = require('./couch.js');


router.get('/test',function(request, response) {
	console.log(" access test called");
	
	var options = {
			host : 'data.mpx.theplatform.com',
			path : "cds/web/Login/loginUser?schema=2.0&form=json&_env=prod&lang=en-US&token="
			+ token
		};
	
	http.request(options, function(resp) {
		var str = '';

		resp.on('data', function(chunk) {
			str += chunk;
		});

		resp.on('end', function() {
			var json = JSON.parse(str);
			if(json.signInResponse){
				db.couchDb.insert(request.query.userName, json, function(result){
					response.send(result);
				});
			}else{
				response.send({
					isErr : "true", 
					status : "400", 
					msg : json });
				}
		});

	}).end();
	//http://data.mpx.theplatform.com/cds/web/Login/loginUser?schema=2.0&form=json&token=<TOKEN>&_env=prod&lang=en-US
});

module.exports = router;
