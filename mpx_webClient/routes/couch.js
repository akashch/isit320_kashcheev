/**
 * @name: mpx WebClient
 * @author: Andrey Kashcheev
 * @module: couch
 */

var request = require('request');
var nano = require('nano')('http://localhost:5984');

var couchDb = (function() {
	'use strict';

	var _instance = null;
	var nanoDb = null;
	var db = {
		dbUrl : 'http://localhost:5984/',
		dbName : "mpx_tokens"
	};

	function couchDb() {
		if (_instance === null) {
			_instance = this;

			Object.defineProperty(this, "insert", {
				get : function() {
					return insert;
				}
			});
			
			Object.defineProperty(this, "read", {
				get : function() {
					return read;
				}
			});

			init();
		} else {
			return _instance;
		}
	}

	function init() {
		createDb();
		nanoDb = nano.db.use(db.dbName);

	}

	function createDb() {

		request.head(db.dbUrl + "" + db.dbName,
				function(error, response, body) {
					if (response.statusCode !== 200) {
						request.put(db.dbUrl + "" + db.dbName, function(error,
								response, body) {
							console.log(response.statusCode
									+ ": database has been created");
						});
					} else {
						console.log("database is already exists");
					}
				});
	}

	function insert(docName, docData, callback) {
		console.log("couchdb - insert method");// debug

		nanoDb.insert(docData, docName, function(err, body) {
			if (!err) {
				console.log(body);
				callback({
					isErr : "false",
					status : 201,
					msg : "success"
				});
			} else {
				console.log(err);
				callback({
					isErr : "true",
					status : 500,
					msg : err
				});
			}
		});

	}

	function read(docName, callback) {
		console.log("Couch - Read Called");
		

		nanoDb.get(docName, {
			revs_info : true
		}, function(err, body) {
			if (!err) {
				console.log(body);
				callback({
					isErr : "false",
					status : 200,
					msg : body
				});
			} else {
				callback({
					isErr : "true",
					status : 500,
					msg : err
				});
			}
		});

	}

	return couchDb;

}());

exports.couchDb = new couchDb();
