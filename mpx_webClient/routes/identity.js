/**
 * @name mpx WebClient
 * @author: Andrey Kashcheev
 * @module: identity
 */
var express = require('express');
var router = express.Router();
var request = require('request');
var fs = require('fs');
var http = require("http");
var https = require("https");
var db = require('./couch.js');




router.get('/signIn',function(request, response) {
		console.log("/identity/signIn called");

		var validationResult = validation(request.query.userName,request.query.password);
		
		if (validationResult) {

			var options = {
				host : 'identity.auth.theplatform.com',
				path : "/idm/web/Authentication/signIn?schema=1.0&form=json&username="
				+ request.query.userName
				+ "&password="
				+ request.query.password
			};

			https.request(options, function(resp) {
				var str = '';

				// another chunk of data has been recieved, so
				// append it to `str`
				resp.on('data', function(chunk) {
					str += chunk;
				});

				// the whole response has been recieved, so we just
				// print it out here
				resp.on('end', function() {
					var json = JSON.parse(str);
					if(json.signInResponse){
						db.couchDb.insert(request.query.userName, json, function(result){
							response.send(result);
						});
					}else{
						response.send({
							isErr : "true", 
							status : "400", 
							msg : json });
						}
				});

			}).end();
			
			
		} else {
			response.send(validationResult);
		}
});


router.get('/signOut',function(request, response) {
	console.log("/identity/signOut called");
	
	
	db.couchDb.read(request.query.userName, function(data){
		
		var options = {
				host : 'identity.auth.theplatform.com',
				path : "/idm/web/Authentication/signOut?schema=1.0&form=json&_token=" + data.msg.signInResponse.token
		};
		
		
		https.request(options, function(resp) {
			var str = '';

			resp.on('data', function(chunk) {
				str += chunk;
			});

			resp.on('end', function() {
				var json = JSON.parse(str);
				console.log(json);// debug

			});
		}).end();
		
		
	});
	
	//response.send({"status" : "OK"});
	
});

// TO DO MORE VALIDATION
var validation = function(userName, password) {
	console.log("***inside validation****");// debug
	userName = userName.trim();
	password = password.trim();
	if (userName.substr(0, 4) !== "mpx/") {
		return {
			isErr : true,
			status : 401,
			msg : "The userName invalid"
		};
	} else if (password.length < 6) {
		return {
			isErr : true,
			status : 401,
			msg : "The password invalid"
		};
	} else {
		return {
			isErr : false,
			status : 200,
			msg : "Valid credantials"
		};
	}

};

module.exports = router;