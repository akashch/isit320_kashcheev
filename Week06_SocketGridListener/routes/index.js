var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Week06_SocketTest_Reader' });
});

module.exports = router;
