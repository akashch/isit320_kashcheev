/**
 * Control.js
 */
var socket2Server = null;//I know it is the Evil 
var c;
var ctx;
var blockSize = 10;

$(document).ready(function() {
	'use strict';
	init();
	readFile("Grid000.json");
});

function init(){
	'use strict';
	c = document.getElementById("myCanvas");
	ctx = c.getContext("2d");
	socket2Server = io.connect('http://localhost:30025');
	socket2Server.on('socket_is_connected', function(message) {
		$('#debug').html(message);
	});
}


function readFile(fileName){
	'use strict';
	$.getJSON(fileName, function(data) {
		sendData2Server(data);
		iterate(data, drawGrid);
	});
}


function drawGrid(x,z, npcValue){
	'use strict';
	if(npcValue === 1){
		ctx.fillStyle = "#FF0000";
	}else{
		ctx.fillStyle = "#00FF00";
	}
	ctx.fillRect(x * blockSize, z * blockSize, blockSize, blockSize);
}

function  sendData2Server(data){
	'use strict';
	socket2Server.emit("GridChanged", data);
}

function iterate(gridData, callback) {
	'use strict';
    if (gridData) {
        for (var z = 0; z < gridData.length; z++) {
            for (var x = 0; x < gridData[0].length; x++) {
                callback(x, z, gridData[z][x]);
            }
        }
    }
}


