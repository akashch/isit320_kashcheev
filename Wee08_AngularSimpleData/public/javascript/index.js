/**
 * Controller
 */
var myModule = angular.module("myModule", []);

myModule.factory("simpleFactory", function() {
	"use strict";
	var factory = {};

	var customers = [ {
		name : {
			firstName : "Tom",
			lastName : "Smith"
		},
		city : 'Seattle'
	}, {
		name : {
			firstName : "Kyle",
			lastName : "Simmons"
		},
		city : 'Seattle'
	}, {
		name : {
			firstName : "Jerrad",
			lastName : "Adams"
		},
		city : 'Bellevue'
	}, {
		name : {
			firstName : "Brad",
			lastName : "Baker"
		},
		city : 'Monro'
	} ];

	factory.getCustomers = function() {
		return customers;
	};

	return factory;

});

myModule.controller("MyController", function($scope, simpleFactory) {
	"use strict";
	$scope.customers = simpleFactory.getCustomers();

	$scope.addCustomer = function() {
		$scope.customers.push({
			name : {
				firstName : $scope.newCustomer.name.substr(0,
						$scope.newCustomer.name.indexOf(" ")),
				lastName : $scope.newCustomer.name
						.substr($scope.newCustomer.name.indexOf(' ') + 1),
			},
			city : $scope.newCustomer.city
		});
		$scope.newCustomer.name = "";
		$scope.newCustomer.city = "";
	};

	$scope.deleteCustomer = function() {
		
		var fullName = $scope.removedCustomer.name.substr(0, $scope.removedCustomer.name.indexOf(" ")) + " " + $scope.removedCustomer.name.substr($scope.removedCustomer.name.indexOf(' ') + 1); 
		
		for (var i = 0; i < $scope.customers.length; i++) {
			console.log(fullName);
			console.log(displayFullname(i));
			if (displayFullname(i) === fullName) {
				$scope.customers.splice(i, 1);
			}
		}
		$scope.removedCustomer.name = "";
		
	};

	function displayFullname(i){
			return $scope.customers[i].name.firstName + " " + $scope.customers[i].name.lastName;
	}

});
