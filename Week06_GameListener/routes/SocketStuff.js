/**
 * SocketStuff.js
 */
module.exports = {

    init: function(server) {
        console.log("init called");

        var io = require('socket.io').listen(server);

        io.sockets.on('connection', function(socket) {
            socket.emit('socket_is_connected', 'Listener is connected to the server!');

            socket.on('disconnect', function() {
                console.log('disconnected event');
                socket.emit('disconnected', 'Listener is disconnected from the game!');
            });

            socket.on('debug', function(data) {
                console.log("debug");
                socket.emit('debug', data);
            });

            socket.on('chatMessage', function(msg){
                console.log('Chat socket message: ', msg);        
                if (msg === "") {
                    msg="Empty message sent.";
                }
                io.sockets.emit('chatMessage', msg);
            });
        
        });

    },

};
