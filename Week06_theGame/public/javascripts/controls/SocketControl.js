/**
 * SocketControl.js (Singelton)
 */
define(["Core"], function(Core) {
    'use strict';

    var socketGame = null;
    var socketListener = null;

    var _instance = null;
    var core = null;

    function SocketControl() {

        if (_instance === null) {
            _instance = this;

            core = new Core();
            init();
        } else {
            return _instance;
        }

    }

    function init() {
        socketGame = io.connect('http://localhost:30025');

        socketListener = io.connect('http://localhost:30026');

        socketListener.on('socket_is_connected', function(message) {
            console.log(message);
        });

        socketGame.on('socket_is_connected', function(message) {
            console.log(message);
        });


    }

    SocketControl.prototype.sendData2Listener = function(position) {
        socketGame.emit("sendData2Listener", {
            "position": position,
            "grid": core.grid,
            "npcs": core.npcs
        });
    };

    return SocketControl;
});
