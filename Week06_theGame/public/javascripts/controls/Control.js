/**
 * Control.js
 */
define(["Core", "SocketControl", "Floors", "PointerLockSetup", "Buildings", "Particles",
    "utilities"
], function(Core, SocketControl, Floors, PointerLockSetup, Buildings,
    Particles, utilities) {
    'use strict';
    var gridFileName = null;
    var npcsFileName = null;
    var lvl = null;
    var gameMenu = null;

    var data = null;

    var core = null;
    var controls = null;
    var animateNpc = false;

    var particles = null;
    var buildings = null;
    var position = null;
    var floors = null;

    var socketCtrl = null;


    function Control(level) {
        lvl = utilities.getLevelNumber(level.slice(3, level.length)); //TODO : FOR GAME MENU 
        init();
    }

    function init() {
        $.getJSON("GameData.json", function(refData) {
            data = refData;

            instantiate();

            doPointerLock();

            socketCtrl = new SocketControl();

            animate();

            window.addEventListener('resize', onWindowResize, false);
            $("#details").html("1. Collect all items </br> 2. Find the door to the next level");

        });
    }

    function instantiate() {
        core = new Core();
        /* jshint ignore:start */
        // setting GameData provided in csv
        core.size = parseInt(data[0]["Value01"]); // TO DO
        gridFileName = data[5]["Value0" + lvl];
        npcsFileName = data[6]["Value0" + lvl];
        // ********************************
        /* jshint ignore:end */
        particles = new Particles();
        particles.initNpc(npcsFileName);

        buildings = new Buildings();
        buildings.addCubes(gridFileName, false);

        floors = new Floors(data);
        floors.drawFloor();
        $.subscribe("loadNewLevel", loadNewLevel);
    }


    function loadNewLevel(event, eventArgs) {
        console.log(eventArgs.message);
        $("#result").empty();
        gridFileName = data[5][eventArgs.value];
        npcsFileName = data[6][eventArgs.value];

        particles = new Particles();
        particles.initNpc(npcsFileName);

        buildings = new Buildings();
        buildings.addCubes(gridFileName, false);
    }

    function doPointerLock() {
        controls = new THREE.PointerLockControls(core.camera);

        var yawObject = controls.getObject();
        core.scene.add(yawObject);

        // Move camera to the 1, 1 position
        yawObject.position.x = core.size;
        yawObject.position.z = core.size;

        var ps = new PointerLockSetup(controls);


    }

    function animate() {

        var controlObject = controls.getObject();
        position = controlObject.position;


        socketCtrl.sendData2Listener(position);
        reDrawTinyMap();

        requestAnimationFrame(animate);
        var xAxis = new THREE.Vector3(1, 0, 0);
        particles.rotateParticlesAroundWorldAxis(0, xAxis, Math.PI / 180,
            animateNpc);
        animateNpc = !animateNpc;

        controls.isOnObject(false);

        buildings.collisionDetection(position, controls);

        // Move the camera
        controls.update();

        core.renderer.render(core.scene, core.camera);

    }

    function reDrawTinyMap() {

        // MAP
        utilities.comparePisiton(position);

        utilities.iterate(core.grid, function(i, j) {
            if (core.grid[i][j] === 1) {
                utilities.drawTinyMap(i, j, "#FF0000");
            } else {
                utilities.drawTinyMap(i, j, "#0000FF");
            }
        });

        // Player
        utilities.drawTinyMap(Math.round(position.x) / core.size, Math.round(position.z) / core.size, "#00FFFF");

        // NPCS
        utilities.iterate(core.npcs, function(i, j) {
            if (core.npcs[j][i] !== 0) {
                utilities.drawTinyMap(i, j, "#FFFF00");
            }
        });
    }

    function onWindowResize() {
        core.camera.aspect = window.innerWidth / window.innerHeight;
        core.camera.updateProjectionMatrix();
        core.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    return Control;
});
