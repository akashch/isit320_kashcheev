/**
 * Utilities.js
 */
define(['Core'], function(Core) {
    'use strict';
    var utilities = {
        showDebug: function(data) {
            console.log(data);
        },
        showError: function(request, ajaxOptions, thrownError) {
            showDebug("Error occurred: = " + ajaxOptions + " " + thrownError);
            showDebug(request.status);
            showDebug(request.statusText);
            showDebug(request.getAllResponseHeaders());
            showDebug(request.responseText);
        },
        getName: function(baseName, x, z) {
            return baseName + x + z;
        },
        iterate: function(arr, callback) {
            for (var i = 0; i < arr.length; i++) {
                for (var j = 0; j < arr[i].length; j++) {
                    callback(i, j);
                }
            }
        },
        comparePisiton: function(playerPosition) {
            var core = new Core();
            var xValue = Math.round(playerPosition.x / core.size);
            var zValue = Math.round(playerPosition.z / core.size);

            if (core.npcs.length > 0 && zValue > -1 && xValue > -1) {
                if (core.npcs[zValue][xValue] !== 0) {
                    xValue = xValue * core.size;
                    zValue = zValue * core.size;
                    $.publish('updateTinyMap', {
                        message: "NPC was removed from scene",
                        xPosition: xValue,
                        zPosition: zValue
                    });

                    core.npcs[zValue / core.size][xValue / core.size] = 0; //remove from grid					
                }
            }

            if (core.endOfTheGame && Math.round(playerPosition.x / core.size) > -1 &&
                Math.round(playerPosition.z / core.size) > -1 &&
                core.grid[0] &&
                core.grid[Math.round(playerPosition.x / core.size)][Math.round(playerPosition.z / core.size)] === 5) {
                $.publish('removeCompletedLevel', {
                    message: "REMOVED COMPLETED LEVEL"
                });
            }

        },
        drawTinyMap: function(i, j, colorHex) {
            var c = document.getElementById("myCanvas");
            var context = c.getContext("2d");
            context.fillStyle = colorHex;
            context.fillRect(i * 10, j * 10, 10, 10);
        },
        getLevelNumber: function(level) {
            if (level.slice(0, 1) === "0") {
                return this.getLevelNumber(level.slice(1, level.length));
            } else {
                return level;
            }
        },
        clearScene: function(baseName, i, j) {
            $.publish('removeObject', {
                "baseName": baseName,
                "i": i,
                "j": j
            });
        }
    };

    return utilities;
});
