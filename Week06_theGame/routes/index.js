var express = require('express');
var router = express.Router();
var csv = require("./csv");


var csvs = ["GameData.csv", "GameData.tsv"];
var json = csv.parse("public/" + csvs[0], 'csv');
csv.writeHtml("GameData.html", json);
csv.writeJson("GameData.json", json);

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', {
        title: 'Week06_theGame'
    });
});


module.exports = router;
