var fs = require('fs');

exports.parse = function(filename, fileType) {

    var splitChar = "\t";

    switch (fileType) {
        case 'csv':
            splitChar = ",";
            break;

        case 'tsv':
            splitChar = "\t";
            break;
    }

    var csv = fs.readFileSync(filename).toString().split("\n");

    var json = [];

    var tokens = csv[0].split(splitChar);
    for (var i = 1; i < csv.length; i++) {
        var content = csv[i].split(splitChar);
        // console.log('length: ' + content);
        var tmp = {};
        for (var j = 0; j < tokens.length; j++) {
            try {
                tmp[tokens[j]] = content[j];
            } catch (err) {
                tmp[tokens[j]] = "";
            }
        }
        json.push(tmp);
    }

    console.log("Parsed Items: " + json.length);
    // this.json = json;
    return json;
};

exports.writeJson = function(fileName, json) {
    fs.writeFile("public/" + fileName, JSON.stringify(json, null, 4), function(err) {
        if (err)
            console.log(err);
        else
            console.log(fileName + " saved.");
    });
};


exports.writeHtml = function(fileName, json) {

    var html = '<div id="GameData">';

    var keyNames = [];
    if (json.length !== 0) {
        for (var key in json[0]) {
            keyNames.push(key);
        }
    }

    for (var i = 0; i < json.length; i++) {
        var name = "";
        var value = "";
        var j = 1;

        for (var elem in json[i]) {
            if (elem === "Name") {
                name = "<strong>" + json[i][elem] + "</strong>";
            } else if (json[i][elem].trim().length !== 0) {
                value += "<li id='Value0" + j + "'>" + json[i][elem] + "</li>";
                j++;
            }

        }
        html += "<p id='" + json[i].Name + "'>" + name + " " + value + "</p> \n";
    }
    html += '</div>';

    fs.writeFile("public/" + fileName, html, function(err) {
        if (err)
            console.log(err);
        else
            console.log(fileName + " saved.");
    });

};
