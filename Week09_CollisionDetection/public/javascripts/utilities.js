/**
 * Utilities.js
 */
define([ 'Core' ], function(Core) {
	'use strict';
	var utilities = {
		showDebug : function(data) {
			console.log(data);
		},
		showError : function(request, ajaxOptions, thrownError) {
			showDebug("Error occurred: = " + ajaxOptions + " " + thrownError);
			showDebug(request.status);
			showDebug(request.statusText);
			showDebug(request.getAllResponseHeaders());
			showDebug(request.responseText);
		},
		getName : function(baseName, x, z) {
			return baseName + x + z;
		},
		iterate : function(arr, callback) {
			for (var i = 0; i < arr.length; i++) {
				for (var j = 0; j < arr[i].length; j++) {
					callback(i, j);
				}
			}
		},
		comparePisiton : function(playerPosition) {
			var core = new Core();
			var xValue = Math.round(playerPosition.x / core.size);
			var zValue = Math.round(playerPosition.z / core.size);

			if (core.npcs.length > 0 && zValue > -1 && xValue > -1) {
				if (core.npcs[zValue] && core.npcs[zValue][xValue] && core.npcs[zValue][xValue] !== 0) {
					xValue = xValue * core.size;
					zValue = zValue * core.size;
					$.publish('updateTinyMap', {
						message : "NPC was removed from scene",
						xPosition : xValue,
						zPosition : zValue
					});
					$.publish("bigEvent", {
						"message" : "New Level",
						"details" : "Player just found NPC"
					});
					core.npcs[zValue / core.size][xValue / core.size] = 0;
				}
			}
			/* jshint ignore:start */
			if (core.endOfTheGame
					&& Math.round(playerPosition.x / core.size) > -1
					&& Math.round(playerPosition.z / core.size) > -1
					&& core.grid[0]
					&& core.grid[Math.round(playerPosition.x / core.size)][Math
							.round(playerPosition.z / core.size)] === 5) {
				$.publish('removeCompletedLevel', {
					message : "REMOVED COMPLETED LEVEL"
				});
			}
			/* jshint ignore:end */

		},
		drawTinyMap : function(i, j, colorHex) {
			var c = document.getElementById("myCanvas");
			var context = c.getContext("2d");
			context.fillStyle = colorHex;
			context.fillRect(i * 10, j * 10, 10, 10);
		},
		drawTriangle : function(i, j, radian, colorHex) {
			// Thank you JSFIDDLE: http://jsfiddle.net/sadasant/3sBRh/4/
			var c = document.getElementById("myCanvas");
			var context = c.getContext("2d");
			var v = [ [ 0, -7 ], [ -7, 0 ], [ 7, 0 ] ];

			context.save();

			context.translate(10 * i + 5, 10 * j + 5);
			context.rotate(radian);

			context.beginPath();
			context.fillStyle = colorHex;

			/*
			 * context.moveTo(10*i,10*j); context.lineTo(10*i,10*j + 10);
			 * context.lineTo(10*i + 10, 10*j + 5);
			 */

			context.moveTo(v[0][0], v[0][1]);
			context.lineTo(v[1][0], v[1][1]);
			context.lineTo(v[2][0], v[2][1]);

			context.closePath();
			context.stroke();
			context.fill();
			context.restore();
		},
		cleanCanvas : function() {
			var c = document.getElementById("myCanvas");
			var context = c.getContext("2d");
			context.clearRect(0, 0, c.width, c.height);
		},
		getLevelNumber : function(level) {
			if (level.slice(0, 1) === "0") {
				return this.getLevelNumber(level.slice(1, level.length));
			} else {
				return level;
			}
		},
		clearScene : function(baseName, i, j) {
			$.publish('removeObject', {
				"baseName" : baseName,
				"i" : i,
				"j" : j
			});
		},
		isDefined : function(variable) {
			return typeof variable !== 'undefined';
		},
		isEmpty : function(arr) {
			return arr.length === 0;
		},
		isEqual : function(val1, val2) {
			return val1 === val2;
		},
		average : function(){
			
			
		}
	};

	return utilities;
});
