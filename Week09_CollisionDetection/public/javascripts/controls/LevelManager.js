/**
 * LevelManager.js
 */
define([ "Core", "SocketControl", "DataContainer", "Floors",
		"PointerLockSetup", "Buildings", "Particles", "utilities" ], function(
		Core, SocketControl, DataContainer, Floors, PointerLockSetup,
		Buildings, Particles, utilities) {
	'use strict';

	var lvl = null;

	var floors = null;
	var buildings = null;
	var particles = null;
	var data = null;

	var scoreArray = [];
	var numberOfRevealed = 0;
	var score = 0;

	var gridFileName = null;
	var npcsFileName = null;

	var dataContainer = null;
	var core = null;

	

	var _instance = null;

	function LevelManager(level) {
		if (_instance === null) {
			_instance = this;
			lvl = utilities.getLevelNumber(level.slice(3, level.length)); // TODO

			Object.defineProperty(this, "floors", {
				get : function() {
					return floors;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "buildings", {
				get : function() {
					return buildings;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "floors", {
				get : function() {
					return floors;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "particles", {
				get : function() {
					return particles;
				},
				enumerable : true,
				configurable : true
			});

			Object.defineProperty(this, "score", {
				get : function() {
					return score;
				},
				enumerable : true,
				configurable : true
			});
			init();

		} else {
			return _instance;
		}
	}

	function init() {
		core = new Core();
		dataContainer = new DataContainer();
		data = dataContainer.data;

		/* jshint ignore:start */
		// setting GameData provided in csv
		core.size = parseInt(data[0]["Value01"]); // TO DO
		gridFileName = data[5]["Value0" + lvl];
		npcsFileName = data[6]["Value0" + lvl];
		// ********************************
		/* jshint ignore:end */
		loadLevel();

		$.subscribe("loadNewLevel", loadNewLevel);
		$.subscribe("fillScoreArray", updateScoreArray);

	}
	
	function loadLevel(){
		$("#levelNumber").html("<strong>Level : "+ gridFileName.slice(11, -5) + " </strong>");
		utilities.cleanCanvas();
		particles = new Particles();
		particles.initNpc(npcsFileName);

		buildings = new Buildings();
		buildings.addCubes(gridFileName, false);
		
		floors = new Floors(data);
		floors.drawFloor();
		$.publish("setOnStart", {"message" : "Put camera on start position"});
	}

	function updateScoreArray() {

		for (var i = 0; i < core.grid.length; i++) {
			scoreArray[i] = [];
			for (var j = 0; j < core.grid[0].length; j++) {
				scoreArray[i][j] = 0;
			}
		}

		$.subscribe("updateScore", function(event, eventArgs) {
			scoreArray[eventArgs.xValue][eventArgs.zValue] = 1;
		});
	}

	function loadNewLevel(event, eventArgs) {
		calculateScore();
		$("#result").empty();
		$("#result").html("<strong>Your score for Level" +  gridFileName.slice(11, -5) + " is "  + Math.round(score) +  "</strong>");
		gridFileName = data[5][eventArgs.value];
		npcsFileName = data[6][eventArgs.value];
		loadLevel();
		$.publish("bigEvent", {"message" : "New Level",
						"details" : "Player just started a new level", "isStarted" : true });
	}
	
	function calculateScore(){
		utilities.iterate(scoreArray, function(i, j) {
			if (scoreArray[i][j] === 1) {
				numberOfRevealed++;
			}
		});
		score = numberOfRevealed/core.originalNumberOfNpcs;
	}

	return LevelManager;
});