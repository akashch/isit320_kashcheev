/**
 * ChatControl.js
 */
define([ require ], function() {
	'use strict';
	
	
	var _instance = null;
	
	function ChatControl(){
		
		if (_instance === null) {
			_instance = this;
			init();
		} else {
			return _instance;
		}
	}
	
	function init(){
		
		$.subscribe("chatMessageRecieved", displayChatMessage);
		$.subscribe("bigEvent", send2Listener);
	}
	
	function displayChatMessage(event, eventArgs){
		$('#chatMessages').prepend($('<li>').text(eventArgs.message));
	}
	
	function send2Listener(event, eventArgs){
		$.publish('sendChatMessage2Listener', {"message" : eventArgs.message, "details" : eventArgs.details , "isStarted" : eventArgs.isStarted});
	}
	
	
	return ChatControl;
});