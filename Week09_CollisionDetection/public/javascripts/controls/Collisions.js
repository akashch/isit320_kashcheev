/**
 * w * Collisions.js
 */
define([ 'Core' ],
		function(Core) {
			'use strict';

			var core = null;
			var _instance = null;

			function Collisions() {
				if (_instance === null) {
					_instance = this;
					init();
				} else {
					return _instance;
				}
			}

			function init() {
				core = new Core();
			}

			Collisions.prototype.collisionDetection = function(position,
					controls) {

				var rays = [ new THREE.Vector3(0, 0, 0.1),
						new THREE.Vector3(0.1, 0, 0.1),
						new THREE.Vector3(0.1, 0, 0),
						new THREE.Vector3(0.1, 0, -0.1),
						new THREE.Vector3(0, 0, -0.1),
						new THREE.Vector3(-0.1, 0, -0.1),
						new THREE.Vector3(-0.1, 0, 0),
						new THREE.Vector3(-0.1, 0, 0.1),
						new THREE.Vector3(0, -0.1, 0),
						new THREE.Vector3(0, -0.1, 0.1),
						new THREE.Vector3(0.1, -0.1, 0),
						new THREE.Vector3(-0.1, -0.1, 0),
						new THREE.Vector3(0, -0.1, -0.1), ];

				var rayHits = [];

				for (var i = 0; i < rays.length; i++) {
					core.raycaster.set(position, rays[i]);
					var intersections = core.raycaster
							.intersectObjects(core.cubes);
					if (intersections.length > 0) {

						controls.isOnObject(true);
						rayHits.push(i);
					}

				}
				if (rayHits.length > 0) {
					var dir = controls.getDirection(new THREE.Vector3(0, 0, 0))
							.clone();
					bounceAway(position, rays, rayHits);
				}

				return false;
			};

			function bounceAway(position, rays, rayHits) {
				var choice = 0;
				if (rayHits.lenght > 1) {
					choice = utilities.average(rayHits);
				}
				var hitChoice = rayHits[choice];

				var newDir = (hitChoice + 3) % 7;
				doMove(position.x + (rays[newDir].x), position.z
						+ (rays[newDir].z), position)

			}

			function doMove(newX, newZ, position) {
				position.x = newX;
				position.z = newZ;

			}

			return Collisions;

		});