/**
w  * Buildings.js
 */
define(["Core", "utilities"], function(Core, utilities) {
    'use strict';
    var scene = null;
    // var cubes = [];
    var cube = null;

    var core = null;
    var flag = true;
    
    function Buildings() {
        core = new Core();
        core.cubes = [];
    }

    Buildings.prototype.addCubes = function(fileName, wireFrame) {
        $.getJSON(fileName, function(data) {
            utilities.iterate(data, function(i, j) {
                if (data[i][j] === 1) {
                    core.grid = data;
                    addCube(wireFrame, core.size * i, 20 * j, 20 * j / 100);
                }
            });
            addLights();
            $.publish("fillScoreArray", null);
        });
    };

    function addCube(wireFrame, x, z, dz) {
        var geometry = new THREE.BoxGeometry(core.size, core.size, core.size);
        var material = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture('images/crate.jpg')
        });
        var cube = new THREE.Mesh(geometry, material);
        if (typeof dz === "undefined") {
            cube.position.set(x, 10, z);
        } else {
            cube.position.set(x, 10, z + dz);
        }
        cube.name = utilities.getName("Cube", x, z);
        core.scene.add(cube);

        core.cubes.push(cube);

        return cube;
    }

    function addLights() {
        var light = new THREE.DirectionalLight(0xffffff, 1.5);
        light.position.set(1, 1, 1);
        core.scene.add(light);
        light = new THREE.DirectionalLight(0xffffff, 0.75);
        light.position.set(-1, -0.5, -1);
        core.scene.add(light);
    }

    function addSphere(wireFrame, x, y) {
        var geometry = new THREE.SphereGeometry(0.5, 25, 25);
        var material = new THREE.MeshNormalMaterial({
            color: 0x00ffff,
            wireframe: wireFrame
        });

        var sphere = new THREE.Mesh(geometry, material);
        sphere.overdraw = true;
        sphere.position.set(x, 1.6, y);
        core.scene.add(sphere);

        return sphere;
    }

    return Buildings;

});
