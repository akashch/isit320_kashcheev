/**
 * main.js
 */
require.config({
	baseUrl : '.',
	paths : {
		"jquery" : 'javascripts/lib/jquery',
		"TinyPubSub" : "javascripts/lib/TinyPubSub",
		"Three" : 'javascripts/lib/three',
		"MTLLoader" : 'javascripts/lib/MTLLoader',
		"OBJMTLLoader" : 'javascripts/lib/OBJMTLLoader',
		"ColladaLoader" : 'javascripts/lib/ColladaLoader',
		"Core" : "javascripts/controls/Core",
		"ChatControl" : "javascripts/controls/ChatControl",
		"GameMenu" : "javascripts/GameMenu",
		"Collisions" : "javascripts/controls/Collisions",
		"Control" : 'javascripts/controls/Control',
		"DataContainer" : "javascripts/controls/DataContainer",
		"LevelManager" : 'javascripts/controls/LevelManager',
		"SocketControl" : "javascripts/controls/SocketControl",
		"Floors" : 'javascripts/draw/Floors',
		"PointerLockControls" : "javascripts/controls/PointerLockControls",
		"PointerLockSetup" : "javascripts/lib/PointerLockSetup",
		"utilities" : "javascripts/utilities",
		"Particles" : "javascripts/draw/Particles",
		"Buildings" : "javascripts/draw/Buildings",
		"Shapes" : "javascripts/draw/Shapes"
	},
	shim : {
		'Three' : {
			exports : 'Three'
		},
		'PointerLockControls' : {
			exports : 'PointerLockControls'
		},
		'MaterialLoader' : {
			exports : 'MTLLoader'
		},
		'OBJMTLLoader' : {
			exports : 'OBJMTLLoader'
		},
		'ColladaLoader' : {
			exports : 'ColladaLoader'
		},
		"TinyPubSub" : {
			deps : [ "jquery" ],
			exports : "TinyPubSub"
		}

	}
});

require([ 'Three', 'Control'], function( Three, Control) {
	'use strict';

	require([ 'jquery', 'TinyPubSub', 'Collisions' ], function(jq, TinyPubSub, Collisions) {

		$(document).ready(function() {
			var control = new Control();
		});

	});

});
