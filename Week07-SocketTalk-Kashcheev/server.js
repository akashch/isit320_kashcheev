var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var io = require('socket.io')(http);

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
	'use strict';
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	'use strict';
    console.log('connection called');    
    socket.on('chatMessage', function(msg){
        console.log('Chat socket message: ', msg);        
        if (msg === "") {
            msg="Empty message sent.";
        }
        io.sockets.emit('chatMessage', msg);
    });
});

http.listen(30025, function(){
	'use strict';
  console.log('listening on *:30025');
});