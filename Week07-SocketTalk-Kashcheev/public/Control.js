/**
 * Control.js
 */


$(document).ready(function(){
	'use strict';
	socket = io('localhost');
	socket.on('chatMessage', function(msg){
	     $('#info').prepend($('<li>').text(msg));
	});
	
	$("#chatButton").click(function(){
		socket.emit('chatMessage', $('#myInput').val());
		$("#myInput").val("");
	});
});