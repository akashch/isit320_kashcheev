/**
 * Control.js
 */
define([ "Floors", "PointerLockSetup", "Buildings" ], function(Floors,
		PointerLockSetup, Buildings) {
	'use strict';

	var scene = null;
	var renderer = null;
	var camera = null;

	var buildings = null;

	var cubes = [];

	var raycaster = null;
	var controls = null;

	var util = null;

	var floors = null;
	var keyMove = {
		moveForward : false,
		moveBackward : false,
		moveLeft : false,
		moveRight : false,
		jump : false
	};

	var cameraPosition = {
		x : 0,
		y : 0,
		z : 0
	};
	

	function Control(utilRef) {

		init(utilRef);
	}

	function init(utilRef) {

		$.getJSON("GameData.json", function(data) {

			util = utilRef;
			// setting GameData provided in csv
			size = parseInt(data[0].Value01);
			cameraPosition.x = data[3].Value01;
			cameraPosition.y = data[3].Value02;
			cameraPosition.z = data[3].Value03;
			numberOfCubes = parseInt(data[2].Value01);
			cubesImage = data[1].Value01;
			// ********************************

			var screenWidth = window.innerWidth / window.innerHeight;
			camera = new THREE.PerspectiveCamera(75, screenWidth, 1, 1000);

			scene = new THREE.Scene();
			scene.fog = new THREE.Fog(0xffffff, 0, 750);

			buildings = new Buildings(scene);

			buildings.addCubes(scene, camera, false);
			doPointerLock();

			var floors = new Floors(data, util);
			floors.drawFloor(scene);

			raycaster = new THREE.Raycaster(new THREE.Vector3(),
					new THREE.Vector3(0, -1, 0), 0, 10);

			renderer = new THREE.WebGLRenderer({
				antialias : true
			});

			renderer.setSize(window.innerWidth, window.innerHeight);
			document.body.appendChild(renderer.domElement);

			window.addEventListener('resize', onWindowResize, false);

			$("#gameData").load("GameData.html");
			animate();
		});

	}

	function doPointerLock() {
		controls = new THREE.PointerLockControls(camera);

		var yawObject = controls.getObject();
		scene.add(yawObject);

		// Move camera to the 1, 1 position
		yawObject.position.x = size;
		yawObject.position.z = size;

		var ps = new PointerLockSetup(controls);
	}

	function animate() {

		requestAnimationFrame(animate);
		var xAxis = new THREE.Vector3(1, 0, 0);

		controls.isOnObject(false);

		var controlObject = controls.getObject();
		var position = controlObject.position;

		buildings.collisionDetection(position, raycaster, controls, util);
		// Move the camera
		controls.update();
		
		// DEBUG DATA
		$("#cameraPosition").html("X : " + Math.round(position.x) + " <br>Y : "+ Math.round(position.y) + "<br>Z : "+ Math.round(position.z));
		renderer.render(scene, camera);
	}

	function onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize(window.innerWidth, window.innerHeight);
	}

	return Control;
});