/**
 * main.js
 */
require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/lib/jquery',
        "Three" : 'javascripts/lib/three',
        "Control" : 'javascripts/Control',
        "Floors" : 'javascripts/Floors',
        "PointerLockControls" : "javascripts/lib/PointerLockControls",
        "PointerLockSetup":"javascripts/PointerLockSetup",
        "Utilities":"javascripts/Utilities",
        "Particles" : "javascripts/Particles",
        "Buildings" : "javascripts/Buildings"
    },
    shim : {
        'Three' : {
            exports : 'Three'
        },
        'PointerLockControls' : {
            exports : 'PointerLockControls'
        }
    }
});

require([ 'jquery', 'Three', 'Control', 'Floors', 'PointerLockSetup', 'Utilities', 'Particles', 'Buildings'], 
		function(jq, Three, Control, Floors, PointerLockSetup, Particles, Buildings, util) {

    'use strict';

    $(document).ready(function() {
        var control = new Control(util);
    });

});

