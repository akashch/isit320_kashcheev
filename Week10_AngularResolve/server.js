var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var nano = require('nano')('http://localhost:5984');
var request = require('request');

app.use(express.static(path.join(__dirname, 'public')));

var dbName = "angular_resolve_kashcheev";
var dbUrl = 'http://localhost:5984/';

app.get('/', function(req, res) {
	"use strict";
	res.sendFile(__dirname + '/index.html');
});



function createDb(){
	request.head(dbUrl + "" + dbName, function(error, response, body){
		if(response.statusCode !== 200){
			request.put(dbUrl+ "" + dbName, function(error, response, body){
				console.log(response.statusCode);
			});
		}else{
			console.log("database is already exists");
		}
	});
}
createDb();


app.get('/create', function(request, response) {
	"use strict";
	console.log("create called");
	
	nano.db.create(dbName, function(err, body) {
		if (!err) {
			console.log("database " + dbName + " has been created");
			response.status(201).send({
				"message" : "database " + dbName + " has been created"
			});
		} else {
			console.log("Could not create database");
			console.log(err);
			response.status(500).send(err);
		}
	});
});

app.get("/insert", function(request, response) {
	"use strict";
	console.log("use database");
	var nanoDb = nano.db.use(dbName);

	nanoDb.insert(request.query.docData, request.query.docName, function(err,
			body) {
		if (!err) {
			console.log(body);
			response.status(201).send({
				"message" : "success"
			});
		} else {
			console.log(err);
			response.status(500).send(err);
		}
	});
});

app.get('/read', function(request, response) {
	'use strict';
	console.log("read called");

	var nanoDb = nano.db.use(dbName);
	nanoDb.get(request.query.docName, {
		revs_info : true
	}, function(err, body) {
		if (!err) {
			console.log(body);
			response.send(body);
		} else {
			response.status(500).send(err);
		}
	});
});


app.get('/docNames', function(request, response) {
	'use strict';
	console.log("docNames called");

	var nanoDb = nano.db.use(dbName);
	var result = [];
	nanoDb.list(function(err, body) {
		if (!err) {
			body.rows.forEach(function(doc) {
				result.push(doc.key);
			});
			console.log(result);
			response.send(result);
		} else {
			response.status(500).send(err);
		}
	});
});

http.listen(30025, function() {
	"use strict";
	console.log('CouchDb URL: ' + nano.config.url);
	console.log('listening on http://localhost:30025');
});