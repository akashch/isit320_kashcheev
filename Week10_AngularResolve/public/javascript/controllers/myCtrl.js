/**
 * Controller
 */

var mainModule = angular.module("mainModule", [ 'ngRoute' ]);

mainModule.config(function($routeProvider) {
	"use strict";
	$routeProvider.when("/", {
		controller : "MyCtrl01",
		templateUrl : "partials/view01.html"
	});
	$routeProvider.when("/view01", {
		controller : "MyCtrl01",
		templateUrl : "partials/view01.html"
	});
	$routeProvider.when("/view02", {
		controller : "MyCtrl02",
		templateUrl : "partials/view02.html"
	}).otherwise({
		redirectTo : '/'
	});
});

mainModule.controller("MyCtrl01", function($scope, myFactory) {
	"use strict";

	var npcs = null;

	var promise4npcs = myFactory.getNpcs();
	
	promise4npcs.then(function(json) {
		npcs = json;
		$scope.npcs = json;
	});

	$scope.load2db = function() {
		for (var i = 0; i < npcs.length; i++) {
			myFactory.load2db("Doc" + i, npcs[i]);
		}
	};

});


mainModule.controller("MyCtrl02", function($scope, myFactory) {
	"use strict";
	
	var results = [];
	
	$scope.readAll = function(){
		myFactory.getDocNames().then(function(json) {
			for(var i = 0; i < json.length; i++){
				myFactory.read(json[i]).then(function(json){
					results.push(json);
				});
			}
		});
	};
	
	$scope.npcs = results;
});
