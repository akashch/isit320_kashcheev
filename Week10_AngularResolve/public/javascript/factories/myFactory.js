/**
 * Factory
 */

var mainModule = angular.module("mainModule");

mainModule.factory("myFactory", function($q) {
	"use strict";

	var exports = {};

	// *****************************
	// Create DB on Couch
	// *****************************
	exports.createDb = function(name) {

		$.ajax({
			type : 'GET',
			url : '/create',
			dataType : 'json',
			data : {
				"dbName" : name
			},
			success : function(response) {
				console.log(response.message);
			},
			error : showError
		});

	};
	// *****************************

	// *****************************
	// Getting doc names
	// *****************************

	exports.getDocNames = function() {

		var defers = $q.defer();

		$.ajax({
			type : 'GET',
			url : '/docNames',
			dataType : 'json',
			success : function(response) {
				console.log(response);
				defers.resolve(response);
			},
			error : showError
		});

		return defers.promise;

	};

	// *****************************

	// *****************************
	// Return doc data
	// *****************************
	exports.read = function(docName) {

		var defers = $q.defer();

		$.ajax({
			type : 'GET',
			url : '/read',
			dataType : 'json',
			data : {
				"docName" : docName
			},
			success : function(response) {
				defers.resolve(response);
			},
			error : showError
		});

		return defers.promise;
	};
	// *****************************

	// *****************************
	// Put data on Couch
	// *****************************
	exports.load2db = function(docName, json) {

		$.ajax({
			type : 'GET',
			url : '/insert',
			dataType : 'json',
			data : {
				"docName" : docName,
				"docData" : json
			},
			success : function(response) {
				console.log(response.message);
			},
			error : showError
		});

	};
	// *****************************

	// *****************************
	// Reading and Converting CSV
	// *****************************
	exports.getNpcs = function() {
		var defers = $q.defer();

		$.ajax({
			type : "GET",
			url : "/data/gameData.csv",
			dataType : "text",
			success : function(data) {
				convertCsvToJson(data, 'csv', defers);
			},
			error : showError
		});
		return defers.promise;
	};

	function convertCsvToJson(data, fileType, defers) {

		var splitChar = "\t";

		switch (fileType) {
		case 'csv':
			splitChar = ",";
			break;

		case 'tsv':
			splitChar = "\t";
			break;
		}

		var csv = data.split("\n");

		var json = [];

		var tokens = csv[0].split(splitChar);
		for (var i = 1; i < csv.length; i++) {
			var content = csv[i].split(splitChar);
			// console.log('length: ' + content);
			var tmp = {};
			for (var j = 0; j < tokens.length; j++) {
				try {
					tmp[tokens[j]] = content[j];
				} catch (err) {
					tmp[tokens[j]] = "";
				}
			}
			json.push(tmp);
		}
		defers.resolve(json);
		console.log("Parsed Items: " + json.length);
	}
	// *****************************

	// *****************************
	// utilities
	// *****************************

	var showError = function(request, ajaxOptions, thrownError) {
		console.log("*****************");
		console.log("Error occurred: = " + ajaxOptions + " " + thrownError);
		console.log(request.status);
		console.log(request.statusText);
		console.log(request.getAllResponseHeaders());
		console.log(request.responseText);
		console.log("*****************");
	};

	return exports;

});